//----- included libraries -----
#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OneButton.h>
#include "SSD1306Wire.h"                    // for OLED SSD1306 I2C 4-pin

#include "setup/wlan_data.h";

const char* ssid     = STASSID;
const char* password = STAPSK;
long rssi = WiFi.RSSI();

unsigned int localPort = 59269;            // local port to listen on
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; // buffers for receiving and sending data
char  sendBuffer[] = "Base ready";         // a string to send

//----- OneButton Setup -----
#define PIN_BUTTON D7
OneButton button(PIN_BUTTON, true);

WiFiUDP Udp;

void setup()
{
  Serial.begin( 115200 );
  WiFi.mode( WIFI_STA );
  WiFi.begin( ssid, password );
  Serial.println();
  Serial.println();
  Serial.print( "Connecting to " );
  Serial.println( ssid );

  while ( WiFi.status() != WL_CONNECTED )
  {
    yield();
  }

  Serial.println( "" );
  Serial.println( "WiFi connected" );
  Serial.println( "IP address: " );
  Serial.println( WiFi.localIP() );
  Serial.printf( "UDP server on port %d\n", localPort );
  Udp.begin( localPort );

    //----- Initialize OneButton -----
  button.setPressTicks(1000);
  button.setClickTicks(600);
  button.setDebounceTicks(50);
  button.attachClick(singleclick);
  //button.attachDoubleClick(doubleclick);
  //button.attachLongPressStart(longPressStart);
  //button.attachLongPressStop(longPressStop); not actively used yet
  //button.attachDuringLongPress(longPress); not actively used yet
}


void singleclick()
{
  Udp.beginPacket( broadcast, 59269 );

  String mac_address = WiFi.macAddress();
  String udp_string;
  char buffer[12];

  for( int i=0; i<mac_address.length(); i++ )
  {
    udp_string = udp_string + mac_address[i];
  }
  sprintf( buffer, "%012d", millis() );
  udp_string = udp_string + "/" + (String)buffer + "/" + "1";
  
  Serial.println( "Sending: " + udp_string );

  for( int i=0; i<udp_string.length(); i++ )
  {
    Udp.write( udp_string[i] );
  }
  
  Udp.endPacket();
}

void loop()
{
  button.tick();    // keep watching the push button
}
