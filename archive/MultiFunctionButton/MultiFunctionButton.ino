
#include <OneButton.h>

#define PIN_INPUT D7

// The 2. parameter activeLOW is true, because external wiring sets the button to LOW when pressed.
OneButton button(PIN_INPUT, true);


// setup code here, to run once:
void setup()
{
  Serial.begin(115200);
  button.attachDoubleClick(doubleclick);
  button.attachClick(singleclick);
  button.attachLongPressStart(longPressStart);
  button.attachLongPressStop(longPressStop);
  button.attachDuringLongPress(longPress);
  button.setPressTicks(1000);
  button.setClickTicks(600);
  button.setDebounceTicks(50);
}

void loop()
{
  button.tick();    // keep watching the push button
  if (button.attachClick(singleclick))
  {
    Serial.println("SingleClick performed");
  }
}

void doubleclick()
{
  Serial.println("DoubleClick");
} 

void singleclick()
{
  Serial.println("SingleClick");
}

void longPressStart() {
  Serial.println("Button 1 longPress start");
}

void longPress() {
  Serial.println("Button 1 longPress...");
}

void longPressStop() {
  Serial.println("Button 1 longPress stop");
}
