/*
***Introduction*******************************************************

The game is client-server based, the flags normally start as clients, one flag can be defined as server.
All flags send team changes as broadcasts. The server is listening to these packages and updates the states of all flags constantly.
The server also regularly updates the teamScores and broadcasts the teamScores to all flags to update the flag colours.
Flags which do not send a keepalive will be removed from the Flag struct.

*** Game Data ********************************************************

Two important game data variables are being maintained in this game.
_________________________________________________________
struct Flag
{
  String mac;
  String zeitpunkt;
  String team;
}

The struct Flag contains all currently active Flags with the latest timestamp when they received an update.
The variable starts with empty values and is subsequently filled and maintained.
The mac address is being used as a unique identifier. Unknown macs are being added to the end of the struct
Flags send event driven updates when their team changes, this updates the entry in the Flag struct.

The transmitted strings via UDP have the following format: MAC/MILLISECONDS/TEAM.
Example: 12:F4:8D:E3:47:03/000000058234/1
mac: 17 characters
zeit: 12 characters
team: 1 character
_________________________________________________________
unsigned int teamScores[8]

The varialbe teamScores contains all scores of all teams. A Time-phased event is checking which team owns which flag via incrementScore
Every time incrementScore is executed every team receives points for every flag they currently own in the Flag construct.

***Subroutine Overview ***********************************************

udpListener()       Ongoing       Constantly listen to incoming UDP packages, distinguish packet types by packet size
saveToFlag()        Event         through Flag changes, updating flags with changes
incrementScore()    Time-phased   Incrementing team scores for each flag owned (loop_wait_time)
sendScore()         Time-phased   Central server sending out updated score to all flags (loop_wait_time)
*/

//----- included libraries -----
#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OneButton.h>
#include "SSD1306Wire.h"                    // for OLED SSD1306 I2C 4-pin

#include "setup.h";

const char* ssid     = STASSID;
const char* password = STAPSK;
long rssi = WiFi.RSSI();

unsigned int localPort = 59269;            // local port to listen on
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; // buffers for receiving and sending data
char  sendBuffer[] = "Base ready";         // a string to send

const unsigned int MAX_FLAGS = 8;
const unsigned int loop_wait_time = 5000; // 5 seconds
const unsigned int keepalive_wait_time = 10000; // 10 seconds
unsigned int last_check = millis();
unsigned int timestamp = millis();
unsigned int teamScores[8];

struct Flag
{
  String mac;
  String zeitpunkt;
  String team;
};

Flag flags[MAX_FLAGS];

WiFiUDP Udp;

//----- OneButton Setup -----
#define PIN_BUTTON D7
OneButton button(PIN_BUTTON, true);

// *** SETUP *********************************************************************************************
void setup()
{
  Serial.begin( 115200 );
  WiFi.mode( WIFI_STA );
  WiFi.begin( ssid, password );
  Serial.println();
  Serial.println();
  Serial.print( "Connecting to " );
  Serial.println( ssid );

  while ( WiFi.status() != WL_CONNECTED )
  {
    yield();
  }

  Serial.println( "" );
  Serial.println( "WiFi connected" );
  Serial.println( "IP address: " );
  Serial.println( WiFi.localIP() );
  Serial.printf( "UDP server on port %d\n", localPort );
  Udp.begin( localPort );

    //----- Initialize OneButton -----
  button.setPressTicks(1000);
  button.setClickTicks(600);
  button.setDebounceTicks(50);
  button.attachClick(singleclick);
  button.attachDoubleClick(doubleclick);
  //button.attachLongPressStart(longPressStart);
  //button.attachLongPressStop(longPressStop); not actively used yet
  //button.attachDuringLongPress(longPress); not actively used yet

  for ( int counter=0; counter<8; counter++ )
  {
    flags[counter].mac = "";
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
  }

}

void singleclick()
{
  sendFlagUpdate();
}

void doubleclick()
{
  localFlagUpdate();
}

// Send out an update of team owning the flag
void sendFlagUpdate()
{
  Udp.beginPacket( broadcast, 59269 );

  String mac_address = WiFi.macAddress();
  String udp_string;
  char buffer[12];

  for( int i=0; i<mac_address.length(); i++ )
  {
    udp_string = udp_string + mac_address[i];
  }
  sprintf( buffer, "%012d", millis() );
  udp_string = udp_string + "/" + (String)buffer + "/" + "1";

  for( int i=0; i<udp_string.length(); i++ )
  {
    Udp.write( udp_string[i] );
  }
  Udp.endPacket();
  
  Serial.println("FLAG update sent to remote flag (" + udp_string +")" );
}

// Update server flag
void localFlagUpdate()
{
  String mac_address = WiFi.macAddress();
  String udp_string;
  char buffer[12];

  for( int i=0; i<mac_address.length(); i++ )
  {
    udp_string = udp_string + mac_address[i];
  }
  sprintf( buffer, "%012d", millis() );
  udp_string = udp_string + "/" + (String)buffer + "/" + "2";
  
  saveToFlag( udp_string );

  Serial.println("FLAG update sent to local flag (" + udp_string +")" );
}

// *** Constantly listen to incoming UDP packages, distinguish packet types by packet size ***************
void udpListener()
{
  // read the packet into packetBuffer
  int packetSize = Udp.parsePacket();
  int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
  packetBuffer[n] = 0;
  
  // If received packet is a flag update saveToFlag (packetSize=32)
  if ( packetSize == 32 )
  {
    String packetBufferString = (String)packetBuffer;
    saveToFlag( packetBufferString );
    
    Serial.printf("FLAG update received (%s) from %s:%d size %d\n",
      packetBuffer, Udp.remoteIP().toString().c_str(), Udp.remotePort(), packetSize);
  }
  
  // If received packet is a score update (packetSize=27)
  else if ( packetSize == 27 )
  {
    // Do some display magic with the new score here
    
    Serial.printf("SCORE update received (%s) from %s:%d size %d\n",
      packetBuffer, Udp.remoteIP().toString().c_str(), Udp.remotePort(), packetSize);
  } 
}

// *** Save received data to flag struct *****************************************************************
void saveToFlag( String flagData )
{
  // Test if its a string we can handle, like 12:F4:8D:E3:47:03/000000058234/1
  if ( flagData.substring( 17, 18 ) == "/" and flagData.substring( 30, 31 ) == "/" )
  {
    String mac = flagData.substring( 0, 17 );
    String hit_time = flagData.substring( 18, 30 );
    String team = flagData.substring( 31 );

    boolean found = false;
    int counter=0;
    
    // Check if flag is already known
    for ( int count=0; count<8; count++ )
    {
      // If flag is known, use existing position
      if ( flags[count].mac == mac )
      {
        counter = count;
        found = true;
        if ( debug == 1 )
        {
          Serial.println( "DEBUG: Updating existing flag on position " + (String)counter );
        }
      }
    }

    // If flag is unknown
    if ( found == false )
    {
      // Find first free position for new flag
      for ( int count=0; count<8; count++ )
      {
        if ( flags[count].mac.length() == 0 )
        {
          counter = count;
          break;
        }
      }
      if ( debug == 1 )
      {
        Serial.println( "DEBUG: Saving new flag to first free position " + (String)counter );
      }
    }
    // Save flag
    flags[counter].mac = mac;
    flags[counter].zeitpunkt = hit_time;
    flags[counter].team = team;
  }
}

// Save received data to flag struct
void cleanupFlags()
{
  int counter=0;
  for ( int count=0; count<8; count++ )
  {
    long x = stringToLong(flags[count].zeitpunkt);      // Convert string timestamp of flags to long variable
    
    // If last update time of flag is > keepalive_wait_time remove the flag from the Flag struct
    if ( timestamp - x > keepalive_wait_time && x != 0 )
    {
      counter = count;
      flags[counter].mac = "";
      flags[counter].zeitpunkt = "";
      flags[counter].team = "";
      // Serial.println( "DEBUG: No update received, removing existing flag on position " + (String)counter );
      Serial.printf("DEBUG: No update received, removing flag %d flagtime is %d timestamp is %d\n", counter, x, timestamp);
    }
  }
}

// Increment team scores for each flag owned
void incrementScore()
{
  String udp_string;
  
  int team_int;
  for ( int i=0; i<MAX_FLAGS; i++ )
  {
    if ( flags[i].mac.length() > 0 and flags[i].team.toInt() > 0 )
    {
      team_int = flags[i].team.toInt();
      teamScores[ team_int ] = teamScores[ team_int ] + 1;
    }
  }
}

// Central server sends out updated score to all flags
void sendScore()
{
  // Combine scores into one char buffer with the format 000/000/000/000/000/000/000/000
  // Each triple digit number represents the points for one team, counting from team 1-8
  char buf[32];
  sprintf (buf, "%03i/%03i/%03i/%03i/%03i/%03i/%03i",teamScores[1],teamScores[2],teamScores[3],teamScores[4],teamScores[5],teamScores[6],teamScores[7]);
  Udp.beginPacket( broadcast, 59269);
  Udp.write( buf );
  Udp.endPacket();
}

// Convert a string into a Long variable
long stringToLong(String s)
{
    char arr[13];
    s.toCharArray(arr, sizeof(arr));
    return atol(arr);
}

// MAIN LOOP
void loop()
{
  button.tick();  // keep watching the push button
  udpListener();  // listen to incoming UDP, process input and reply  

  // Repeating actions every <loop_wait_time> seconds
  timestamp = millis();
  if ( ( timestamp - last_check ) > loop_wait_time )
  {
    last_check = timestamp;
    incrementScore(); // Only necessary for server
    sendScore(); // Only necessary for server
    cleanupFlags(); // Only necessary for server
    sendFlagUpdate(); // Only necessary for clients
  }
  
}
