/* This is a sketch to simulate an IR-shot from a Recoil gun. The modulation of 38KHz is carried out by the void pulse().
   A modulation of 26 microseconds results in a frequency of 38,000 per second (1000 / 38).
   The Recoil IR-protocol operates in segments of 400µs (header 3200µs, gap 1600µs, 1 is 800µs and 0 is 2x 400µs modulating.

   Base for this test was an ESP8266 WEMOS D1 Mini.
   You may use and develop this sketch at your wish.
   2021 - Andre West
*/

//----- included libraries -----
#include <OneButton.h>

//----- OneButton Setup -----
#define PIN_BUTTON D7
OneButton button(PIN_BUTTON, true);
typedef enum { ACTION_NONE, ACTION_SINGLE, ACTION_DOUBLE, ACTION_LONGSTART, ACTION_PRESS, ACTION_LONGSTOP } MyActions;
MyActions nextAction = ACTION_NONE;

//----- IR Setup -----
const int IRLedPin = D1;

//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------void setup---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void setup()
{
  pinMode(IRLedPin, OUTPUT);

  Serial.begin( 115200 );           // Initialize Serial Console
  pinMode( LED_BUILTIN, OUTPUT );
  digitalWrite( LED_BUILTIN, HIGH );

  //----- Initialize OneButton -----
  button.setPressTicks( 1000 );
  button.setClickTicks( 600 );
  button.setDebounceTicks( 50 );
  button.attachClick( singleclick );
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- void loop ---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void loop()
{
  button.tick();                   // watching the button

  if ( nextAction == ACTION_SINGLE )
  {
    SendLaserCode();
    nextAction = ACTION_NONE;
  }
  else
  {
    Reset();
  }
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- IR subroutines ----------------------------------------------
void Reset()
{
  digitalWrite (IRLedPin, LOW);
}

//-----------------------------------------------------------------------------------------------------------
void pulseIR(int microsecs)       // sending the pulse for a defined number of microseconds
{
  while (microsecs > 0)
  {
    digitalWrite(IRLedPin, HIGH); // this takes about 3 microseconds to happen
    delayMicroseconds(9);         // this is half the time T=1/f minus 3
    digitalWrite(IRLedPin, LOW);  // this also takes about 3 microseconds
    delayMicroseconds(9);         // this is the other half
    microsecs = microsecs - 26;   // this is the total time for a given freq 26 micro = 1/38000
  }
}

//-----------------------------------------------------------------------------------------------------------
void SendLaserCode() {            // sending code 000100100011000011100 - first shot of player 1

  pulseIR(3200);                  // header and
  delayMicroseconds(1600);        // gap after header to initialize shot

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(800);           // 1

  delayMicroseconds(400); // 0
  pulseIR(400);

  delayMicroseconds(400); // 0
  pulseIR(400);

  delayMicroseconds(800); // 1

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(800);           // 1

  delayMicroseconds(800); // 1

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(400);           // 0
  delayMicroseconds(400);

  pulseIR(800);           // 1

  delayMicroseconds(800); // 1

  pulseIR(800);           // 1

  delayMicroseconds(400); // 0
  pulseIR(400);

  delayMicroseconds(400); // 0
  pulseIR(400);

  Serial.println( "IR sent");
}

//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- OneButton subroutines -------------------------------------------

void singleclick()
{
  Serial.println("SingleClick");
  nextAction = ACTION_SINGLE;
}
