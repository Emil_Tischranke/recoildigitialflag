//-----------------------------------------------------------------------------------------------------------
//---------------------------------------- Standard LED Routines ------------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void led_YES(int n)                   // indicating a green check mark
{
  strip.clear();
  strip.show();
  delay(n);
  // run a blue dot down and leave green behind from the middle
  strip.setPixelColor( 5, blue );
  strip.show();
  delay(n);
  strip.setPixelColor( 5, lightblue );
  strip.setPixelColor( 6, blue );
  strip.show();
  delay(n);
  strip.setPixelColor( 5, green );
  strip.setPixelColor( 6, lightblue );
  strip.setPixelColor( 7, blue );
  strip.show();
  delay(n);
  strip.setPixelColor( 6, green );
  strip.setPixelColor( 7, lightblue );
  strip.setPixelColor( 8, blue );
  strip.show();
  delay(n);
  strip.setPixelColor( 7, green );
  strip.setPixelColor( 8, lightblue );
  strip.setPixelColor( 9, blue );
  strip.show();
  delay(n);
  strip.setPixelColor( 8, green );
  strip.setPixelColor( 9, lightblue );
  strip.show();
  delay(n);
  strip.setPixelColor( 9, green );
  strip.show();
  delay(n);
  // run a blue dot up and leave green behind from the bottom to the top
  for ( int i = strip.numPixels() - 1; i > -3; i-- )
  {
    strip.setPixelColor( i, blue );
    strip.setPixelColor( i + 1, lightblue );
    strip.setPixelColor( i + 2, green );
    strip.show();
    delay(n);
  }
  delay(n * 5);
  strip.clear();
  strip.show();
}

//-----------------------------------------------------------------------------------------------------------
void led_NO()                   // indicating a red No-No-No No
{
  for ( int i = 1; i < 4; i++ )
  {
    strip.fill( red, 0, strip.numPixels());
    strip.show();
    delay(200);
    strip.clear();
    strip.show();
    delay(100);
  }
  strip.fill( red, 0, strip.numPixels());
  strip.show();
  delay(600);
  strip.clear();
  strip.show();
  delay(400);
}

//-----------------------------------------------------------------------------------------------------------
void displayAllColors()
{
  strip.setPixelColor(0, red);
  strip.setPixelColor(1, orange);
  strip.setPixelColor(2, yellow);
  strip.setPixelColor(3, green);
  strip.setPixelColor(4, lightblue);
  strip.setPixelColor(5, cyan);
  strip.setPixelColor(6, blue);
  strip.setPixelColor(7, purple);
  strip.setPixelColor(8, red);
  strip.setPixelColor(9, orange);
  strip.show();
}

//-----------------------------------------------------------------------------------------------------------
// colorWipe from top to bottom
void colorWipe( uint32_t color, int wait )
{
  for ( int i = 0; i < strip.numPixels(); i++ )
  {
    strip.setPixelColor(i, color);
    strip.show();
    delay(wait);
  }
}

//-----------------------------------------------------------------------------------------------------------
// colorWipe from bottom to top
void r_colorWipe( uint32_t color, int wait )
{
  for ( int i = strip.numPixels() - 1; i > -1; i-- )
  {
    strip.setPixelColor( i, color );
    strip.show();
    delay( wait );
  }
  delay(300);
}

//-----------------------------------------------------------------------------------------------------------
void colorBand_bg( int c_time )            // 3 running pixels in green running over a blue band
{
  for ( int t = 0; t < c_time; t++ )
  {
    strip.fill( strip.Color(0, 0, 255), strip.numPixels() - 1, 10 );
    strip.show();
    for ( int n = strip.numPixels() - 2; n > -1; n-- )
    {
      strip.setPixelColor( n, lightblue );
      strip.show();
      strip.setPixelColor( n + 1, green );
      strip.show();
      strip.setPixelColor( n + 2, lightblue );
      strip.show();
      strip.setPixelColor( n + 3, blue );
      strip.show();
      delay( 100);
    }
    for ( int n = 1; n < strip.numPixels() + 1; n++ )
    {
      strip.setPixelColor( n, lightblue );
      strip.show();
      strip.setPixelColor( n - 1, green );
      strip.show();
      strip.setPixelColor( n - 2, lightblue );
      strip.show();
      strip.setPixelColor( n - 3, blue );
      strip.show();
      delay(100);
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// rainbow moving over complete band
void rainbow( int wait )
{
  for ( long firstPixelHue = 0; firstPixelHue < 5 * 65536; firstPixelHue += 256 )
  {
    // For each pixel in strip...
    for ( int i = 0; i < strip.numPixels(); i++ )
    {
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels() );
      strip.setPixelColor( i, strip.gamma32( strip.ColorHSV( pixelHue ) ) );
    }
    strip.show();
    delay( wait );
  }
}

//-----------------------------------------------------------------------------------------------------------
void led_beacon(int color, int t )
{
  if ( currentTime - counterPreviousTime > t )
  {
    strip.clear();
    strip.show();
    strip.fill( color, 0, strip.numPixels() );
    strip.show();
    delay(50);
    strip.clear();
    strip.show();
    delay(50);
    strip.fill( color, 0, strip.numPixels() );
    strip.show();
    delay(50);
    strip.clear();
    strip.show();
    counterPreviousTime = currentTime;
  }
}
