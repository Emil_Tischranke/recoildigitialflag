//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- Menu Routines -----------------------------------------------
//-----------------------------------------------------------------------------------------------------------

int setOperationMode( int newOperationmode, int color )
{
  operationMode = newOperationmode;
  led_currentMenuColor = color;
  oled_OK();
  led_YES(50);
  display.clear();
  display.display();
  menuInit = 0;
  gameMode = 0;
  nextAction = ACTION_NONE;
}

//-----------------------------------------------------------------------------------------------------------
int writeToDisplay( int color, int x, int y, String text, int menu )
{
  if ( menu == 1 )
  {
    led_currentMenuColor = color;
  }
  else
  {
    led_currentMenu2Color = color;
  }
  display.clear();
  display.setFont( ArialMT_Plain_10 );
  display.setTextAlignment( TEXT_ALIGN_LEFT );
  display.drawString( x, y, text );
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
void mainMenu()
{
  strip.clear();
  strip.show();
  strip.setPixelColor( 0, led_currentMenuColor );
  strip.show();

  if ( menuInit == 0 )
  {
    writeToDisplay( blue, 0, 24, ">", 1 );
    menuInit = 1;
  }

  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 0, "Main Menu");
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(12, 24, "1. Stand-alone");
  display.drawString(12, 34, "2. WiFi - Client");
  display.drawString(12, 44, "3. WiFi - Server");
  display.display();

  if ( nextAction == ACTION_SINGLE )
  {
    if ( led_currentMenuColor == blue )
    {
      writeToDisplay( green, 0, 34, ">", 1 );
      menuInput = 1;
    }
    else if ( led_currentMenuColor == green )
    {
      writeToDisplay( red, 0, 44, ">", 1 );
    }
    else if ( led_currentMenuColor == red )
    {
      writeToDisplay( blue, 0, 24, ">", 1 );
    }
    nextAction = ACTION_NONE;
  }

  if ( nextAction == ACTION_DOUBLE && led_currentMenuColor == blue )
  {
    setOperationMode( 1, blue );
    gameMode = 0;
  }
  else if ( nextAction == ACTION_DOUBLE && led_currentMenuColor == green )
  {
    setOperationMode( 2, green );
    gameMode = 3;
  }
  else if ( nextAction == ACTION_DOUBLE && led_currentMenuColor == red )
  {
    server = 1;
    setOperationMode( 3, red );
  }
}

//-----------------------------------------------------------------------------------------------------------
// Duration is considered 1point/1second/flag, updated every loop_wait_time (default 5s)
// If a team owns three flags it receives 3 points/second, or with the wait_time 15 points/5 seconds

void chooseDurationMenu( int displayNr, String gameName, int gameModeAfter )
{
  strip.clear();
  strip.show();
  strip.setPixelColor( 0, led_currentMenuColor );
  strip.setPixelColor( 1, led_currentMenu2Color );
  strip.show();
  if ( menuInit == 0 )
  {
    writeToDisplay( blue, 0, 24, ">", displayNr );
    menuInit = 1;
  }
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString( 63, 0, gameName );
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString( 10, 24, "    5 min" );
  display.drawString( 10, 34, "   10 min" );
  display.drawString( 10, 44, "   15 min" );
  display.display();

  if ( nextAction == ACTION_SINGLE )
  {
    if ( led_currentMenu2Color == blue )
    {
      writeToDisplay( green, 0, 34, ">", displayNr );
    }
    else if ( led_currentMenu2Color == green )
    {
      writeToDisplay( red, 0, 44, ">", displayNr );
    }
    else if ( led_currentMenu2Color == red )
    {
      writeToDisplay( blue, 0, 24, ">", displayNr );
    }
    nextAction = ACTION_NONE;
  }
  if ( nextAction == ACTION_DOUBLE && led_currentMenu2Color == blue )
  {
    // 5min
    maxPoints = 300;
  }
  else if ( nextAction == ACTION_DOUBLE && led_currentMenu2Color == green )
  {
    // 10min
    maxPoints = 600;
  }
  else if ( nextAction == ACTION_DOUBLE && led_currentMenu2Color == red )
  {
    // 15min
    maxPoints = 900;
  }
}

//-----------------------------------------------------------------------------------------------------------
int setGameMode( int newGameMode, boolean oled, boolean led, boolean displayColors )
{
  gameMode = newGameMode;
  if ( oled )
  {
    oled_OK();
  }
  else
  {
    oled_NO();
  }

  if ( led )
  {
    led_YES(50);
  }
  else
  {
    led_NO();
  }

  display.clear();
  display.display();

  if ( displayColors )
  {
    displayAllColors();
  }
  startTime = currentTime;
  menuInit = 0;
  nextAction = ACTION_NONE;
}

//-----------------------------------------------------------------------------------------------------------
void gameMenu( int displayNr, String gameName, int gameModeAfter )
{
  strip.clear();
  strip.show();
  strip.setPixelColor( 0, led_currentMenuColor );
  strip.setPixelColor( 1, led_currentMenu2Color );
  strip.show();
  if ( menuInit == 0 )
  {
    writeToDisplay( blue, 0, 24, ">", displayNr );
    menuInit = 1;
  }
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString( 63, 0, gameName );
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString( 10, 24, "1. Domination" );
  display.drawString( 10, 34, "2. Base Attack" );
  display.drawString( 10, 44, "3. Shoot the Flag" );
  display.display();

  if ( nextAction == ACTION_SINGLE )
  {
    if ( led_currentMenu2Color == blue )
    {
      writeToDisplay( green, 0, 34, ">", displayNr );
    }
    else if ( led_currentMenu2Color == green )
    {
      writeToDisplay( red, 0, 44, ">", displayNr );
    }
    else if ( led_currentMenu2Color == red )
    {
      writeToDisplay( blue, 0, 24, ">", displayNr );
    }
    nextAction = ACTION_NONE;
  }
  if ( nextAction == ACTION_DOUBLE && led_currentMenu2Color == blue )
  {
    // Domination
    setGameMode( 1, true, true, true );
  }
  else if ( nextAction == ACTION_DOUBLE && led_currentMenu2Color == green )
  {
    // Base Attack
    setGameMode( 2, true, true, true );
  }
  else if ( nextAction == ACTION_DOUBLE && led_currentMenu2Color == red )
  {
    // Shoot the flag
    setGameMode( 3, true, true, true );
  }
}
