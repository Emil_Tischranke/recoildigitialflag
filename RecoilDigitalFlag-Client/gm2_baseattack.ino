//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode Base Attack -------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void gameModeBaseAttack()
{
  if ( baseTeam == 0 ) displayAllColors();

  if ( baseTeam != 0 ) countHitPoints();

  if ( ( currentTime - counterPreviousTime ) > loop_wait_time )
  {
    identifyLeadingTeam();
    updateFlagScore();                      // Update local flag score for team owning this flag

    counterPreviousTime = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
void showBase()
{
  if ( ir_shooterID != 0 && baseTeam == 0 )
  {
    baseTeam = actTeam;
    baseColor = led_colorActTeam;
    r_colorWipe( baseColor, 50 );                  // colorWipe carried out, if  flag is hit fist time
  }
  else if ( ir_shooterID != 0 && baseTeam != actTeam)
  {
    baseHit = 1;
  }
  else if (ir_shooterID != 0 && baseTeam == actTeam)
  {
    baseHeal = 1;
  }
}

//-----------------------------------------------------------------------------------------------------------
void countHitPoints()
{
  if ( currentTime - counterPreviousTime >= countIntervall)
  {
    if ( baseHeal == 1 && actHitPoints < maxHitPoints )         // no heal possible when hit points (health of base) are at max
    {
      actHitPoints++;
      baseHeal = 0;
      led_Heal();
      displayBaseHealth();
    }
    else if (baseHit == 1)
    {
      actHitPoints--;
      baseHit = 0;
      led_Hit();
      displayBaseHealth();
    }
    if ( debug == 1 )
    {
      Serial.print("remaining Hit Points : ");
      Serial.println(actHitPoints);
    }

    counterPreviousTime = currentTime;
  }
  else if ( actHitPoints <= 0 )                                 // team that removes the last hitpoint conquers the flag
  {
    winningTeamColor = led_colorActTeam;
    operationMode = 42;

    displayWinner();
  }
  else if ( currentTime - startTime >= playTimeInMillis )       // if time has run out and flag has not been taken the owning team wins the flag (before: currentTime - startTime >= playTimeInMillis )
  {
    winningTeamColor =  baseColor;
    operationMode = 42;

    displayWinner();
  }
}

//-----------------------------------------------------------------------------------------------------------
void displayBaseHealth()
{
  if ( baseTeam != 0 )
  {
    colorWipe( black, 50 );
    strip.fill( baseColor, (strip.numPixels() - actHitPoints), actHitPoints);
    strip.show();
  }
}
