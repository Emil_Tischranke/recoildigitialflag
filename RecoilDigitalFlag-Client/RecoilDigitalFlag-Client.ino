//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Include Libraries ---------------------------------------------
//-----------------------------------------------------------------------------------------------------------

//Flag is only working with ESP8266 board version 2.3.0, later seems not to work
// Adafruit NeoPixel Version 1.8.4 (1.8.5 not working) https://github.com/adafruit/Adafruit_NeoPixel
#include <Adafruit_NeoPixel.h>
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi
#include <ESP8266WiFi.h>
// Wifi 1.2.7 https://www.arduino.cc/en/Reference/WiFi
#include <ESP8266WiFiMulti.h>   // Include the Wi-Fi-Multi library
// https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/ESP8266WiFiMulti.h
#include <WiFiUdp.h>
// OneButton Version 1.5.0 https://github.com/mathertel/OneButton


//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Global Definitions --------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Definition of global variables - values initialized in setup()
// Definition of global constants including values


//----- Network Definitions -----
#define STASSID "Recoil Game Hub_WDS"
#define STAPSK  "recoil123"
const char* ssid     = STASSID;
const char* password = STAPSK;
const unsigned int localPort = 17500;               // local port to listen on - same UDP-port as in the SimpleCoil app
const String mac_address = WiFi.macAddress();
const IPAddress broadcast(255, 255, 255, 255);      // Broadcast Address
long rssi;
int WifiStatus;                                     // the Wifi radio's status prior to connection
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1];      // buffers for receiving and sending data
WiFiUDP Udp;
ESP8266WiFiMulti wifiMulti;

//-----GLOBAL STRINGS-----
String MESSAGE_PREFIX       = "SimpleCoil:";
String NETMSG_STARTGAME     = "STARTGAME";
String NETMSG_ENDGAME       = "ENDGAME";
String NETMSG_FLAGALIVE     = "FLAGALIVE";
String NETMSG_FLAGSCORE     = "FLAGSCORE";
String NETMSG_FLAGRESET     = "FLAGRESET";          // resetting all parameters
String NETMSG_REPLAY        = "REPLAY";             // just resetting counters - keeping flag modes
String NETMSG_TEAMSET       = "TEAMSET";            // telling the last player-ID for each team
String NETMSG_GAMEMODE      = "GAMEMODE";           // new message for multiple games, setting gameMode over UDP
String NETMSG_SHOWFLAG      = "SHOWFLAG";           // carries out a white colorwipe on the adressed flag
String NETMSG_FLAGTAKEN     = "FLAGTAKEN";          // flag has been taken by another team
String NETMSG_FLAGRESTORED  = "FLAGRESTORED";       // time has run out and flag has been restored
String NETMSG_FLAGCAPTURED  = "FLAGCAPTURED";       // flag has been booked in by the taking team


//----- IR Definitions -----
const int IR_PIN = D6;                     // The pin to which the active-low IR sensor is attached
const unsigned long MAX_PULSELENGTH = 200;
const int MAX_PULSES = 100;                // Maximum number of pulses that may be detected before the measurements are printed to serial
const int MAX_MESSAGE_BITS = 100;          // Maximum number of bits per message
const bool PRINT_INVALID_PACKETS = true;   // True to print debugging information about invalid packets, false to skip them.
const int END_OF_PACKET = -1;              // Instead of a time interval, write this value to dts to indicate the end of a packet.
int dts[MAX_PULSES];                       // Microsecond deltas detected between edge transitions
volatile int dtIndex = 0;
byte bits[MAX_MESSAGE_BITS];               // F-2F decoded message bits
byte lastBits[MAX_MESSAGE_BITS];           // F-2F decoded message bits

//----- LED Definitions -----
#define LED_PIN   D1
#define LED_COUNT 10
Adafruit_NeoPixel strip( LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800 );

int LED_BRIGHTNESS = 50;
int led_connectionQuality = int( ( 120 - abs( WiFi.RSSI() ) ) / 10 ); // calculation of number of pixels correlating to signal strength
int led_connectionColor = 0;                                          // connection color
int c = 0;                                                            // counter

// LED color declarations
const unsigned int black =     strip.Color(   0,   0,   0 );
const unsigned int blue =      strip.Color(   0,   0, 255 );
const unsigned int red =       strip.Color( 255,   0,   0 );
const unsigned int green =     strip.Color(   0, 255,   0 );
const unsigned int purple =    strip.Color( 255,   0, 255 );
const unsigned int white =     strip.Color( 255, 255, 255 );
const unsigned int cyan =      strip.Color(  18, 205, 242 );
const unsigned int orange =    strip.Color( 255,  50,   2 );
const unsigned int yellow =    strip.Color( 255, 255,   0 );
const unsigned int lightblue = strip.Color(   0, 150,  80 );

// ----- Team declarations -----
const char *teamColor[] = {"None", "Red", "Blue", "Green", "Purple"};
const int teamColorRGB[] = {black, red, blue, green, purple};

// number of players in team
int lastPlayerTeam1 = 16;
int lastPlayerTeam2 = 32;
int lastPlayerTeam3 = 48;
int lastPlayerTeam4 = 63;

//----- Timing Definitions -----
const unsigned int loop_wait_time = 5000;              // 5 seconds
const unsigned int keepalive_wait_time = 10000;        // 10 seconds
const unsigned long countIntervall = 1000;             // intervall in ms to count points
unsigned long counterPreviousTime;
unsigned long counterPreviousTime2;
unsigned long currentTime;

//----- Game Definitions -----
int operationMode;                                     // 0 = main menu, 3 = "Client", 42 = "Display Winner"
int gameMode;                                          // 0 = game menu, 1 = Domination, 2 = Base Attack, 3 = Shoot The Flag, 4 = Take & Hold, 5 = Capture The Flag
int gameStarted;                                       // variable that indicates game has been started by button or by UDP message
int leadingScore;                                      // highest score during the game
int leadingTeam;                                       // team leading the game currently
int actTeam;                                           // team shooting the local flag
int prevTeam;                                          // team that held the flag prior to a detected shot - can be the same team as actTeam
int winningTeamColor;
int led_ColorWipe;                                     // indicates that a ColorWipe has to be carried out
int baseColor;
int baseTeam;                                          // team that owns the base
int baseHit;                                           // activated if the base it hit by a hostile team
int baseHeal;                                          // activated if the base it hit by the own broadcastteam
int flagTaken;                                         // is set when the flag has been take and is no longer idle mode
int teamID;
int ir_shooterID;
int led_colorActTeam;
int gunShot;                                           // gun shot detected
int flagInPosition = 0;                                // indicates that the flag is in position for game in WiFi mode
int randomNumber;
int changeTime;
int playTime;                                          // play time in minutes
unsigned long playTimeInMillis;                        // conversion of minutes into millis
unsigned long startTime;                               // begin of the game
int startCounting;

int maxPoints;                                         // limit for the maximum points, whoever reaches these points first, wins the game
int actTeamPoints;
int led_Quotient;
int led_Percent;
int flagOwner;                                         // player who captured a flag from another team

float maxHitPoints;
int actHitPoints;

unsigned int teamScores[5];
struct Flag
{
  String mac;
  String zeitpunkt;
  String team;
};
const unsigned int MAX_FLAGS = 8;
Flag flags[MAX_FLAGS];

int debug = 0;


//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------Setup--------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Initialize global variables - this should be done here so a reset can be triggered via resetFunction calling setup()
void setup()
{
  Serial.begin( 115200 );                                  // Initialize Serial Console
  pinMode( LED_BUILTIN, OUTPUT );
  digitalWrite( LED_BUILTIN, HIGH );

  //----- Initialize LED -----
  strip.begin();                                           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.clear();                                           // turn OFF all pixels
  strip.show();                                            // update all pixels
  strip.setBrightness( LED_BRIGHTNESS );                   // Set BRIGHTNESS to about 1/5 (max = 255)
  Serial.println( "LED Strips initialized." );

  //----- Initialize IR -----
  pinMode( IR_PIN, INPUT );
  Serial.println( "Infrared Sensor initialized." );

  // ----- Initialize WiFi -----
  // This is important because it clears still active entries on access points preventing reconnections
  WifiStatus = 0;
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  rssi = WiFi.RSSI();
  // add Wi-Fi networks you want to connect to
  wifiMulti.addAP("Recoil Game Hub_WDS", "recoil123");
  wifiMulti.addAP("Recoil Game Hub", "recoil123");
  wifiMulti.addAP("Moria", ".Mellon.");
  Serial.println( "WiFi initialized." );

  //----- Initialize timing variables -----
  counterPreviousTime = 0;
  counterPreviousTime2 = 0;
  currentTime = millis();

  // ----- Initialize game variables -----
  operationMode = 2;
  gameMode = 0;
  gameStarted = 0;

  leadingScore = 0;
  leadingTeam = 0;
  actTeam = 0;
  prevTeam = 0;                                          // team that held the flag prior to a detected shot - can be the same team as actTeam
  winningTeamColor = 0;
  baseColor = 0;
  baseTeam = 0;
  baseHit = 0;
  baseHeal = 0;
  flagTaken = 0;
  teamID = 0;
  led_colorActTeam = 0;
  gunShot = 0;                                          // gun shot detected
  flagOwner = 0;
  changeTime = 0;

  maxPoints = 0;                                        // still defined for older games, should be variable in the future
  actTeamPoints = 0;
  led_Quotient = 0;                                     // necessary here for older games, dynamically calculated in network mode; strip.numPixels() / maxPoints
  led_Percent = 0;                                      // necessary here for older games; led_Quotient * actTeamPoints


  for ( int counter = 0; counter < 8; counter++ )       // debug for teams 1-8
  {
    flags[counter].mac = "";
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
    teamScores[counter] = 000;
  }
  Serial.println( "Game initialized." );
  rainbow( 3 );                                         // illumination at start of program
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- Main Loop ---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Game logic should be implemented on top logic main loop instead of subroutines

void loop()
{
  readIR();                                    // keep listening to the IR sensor
  currentTime = millis();                      // keep currentTime up-to-date

  wifiConnect();
  udpListener();                               // listen to incoming UDP, process input and reply

  if ( operationMode == 2 )
  {
    if ( flagInPosition == 0 ) displaySignalstrength();

    else if ( flagInPosition == 1 )
    {
      if ( gameStarted == 0 )
      {
        if ( gameMode == 0 )led_beacon(blue, 3000);
        else led_beacon(green, 3000);
        if ( ( currentTime - counterPreviousTime2 ) > loop_wait_time )
        {
          sendFlagUpdate();                     // Regular send flag updates as keepalives and ownership change info
          counterPreviousTime2 = currentTime;
        }
      }

      if ( gameStarted == 1 )
      {
        if ( gameMode == 1 ) gameModeDomination();
        if ( gameMode == 2 ) gameModeBaseAttack();
        if ( gameMode == 3 ) gameModeShootTheFlag();
        if ( gameMode == 4 ) gameModeTakeAndHold();
        if ( gameMode == 5 ) gameModeCaptureTheFlag();
      }
    }
  }


  //--------------------------------------------- End of game ----------------------------------------------
  else if ( operationMode == 42 )
  {
    if ( leadingScore == 0 && gameStarted == 1 ) led_beacon( red, 500 );

    else displayWinner();

    udpListener();
  }
}

//-----------------------------------------------------------------------------------------------------------
//---------------------------------------------- Subroutines ------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void resetFunction() // resetting everything
{
  setup();
}

void replayFunction() // resetting counters
{
  operationMode = 2;
  gameStarted = 0;
  leadingScore = 0;
  leadingTeam = 0;
  actTeam = 0;
  prevTeam = 0;
  baseColor = 0;
  baseTeam = 0;
  teamID = 0;
  actTeamPoints = 0;
  flagTaken=0;
  flagOwner = 0;
  changeTime = 0;
  counterPreviousTime = 0;
  counterPreviousTime2 = 0;
  for ( int counter = 0; counter < 8; counter++ )       // debug for teams 1-4
  {
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
    teamScores[counter] = 000;
  }
}

//-----------------------------------------------------------------------------------------------------------
//---------------------------------------------- Gamemode: Domination ----------------------------------------
//-----------------------------------------------------------------------------------------------------------
void updateActTeam() // Update active team on flag due to a received shot
{

  int ir_shooterID = shooterIndex( bits + 11 );    // detected shooterID from 1 - 64
  if ( ir_shooterID > 0 && ir_shooterID <= lastPlayerTeam1 )
  {
    Serial.println("Team Red ");
    prevTeam = actTeam;
    actTeam = 1;
    led_colorActTeam = teamColorRGB[ actTeam ];
    swapTeam();
  }
  else if ( ir_shooterID > lastPlayerTeam1 && ir_shooterID <= lastPlayerTeam2 )
  {
    Serial.println("Team Blue ");
    prevTeam = actTeam;
    actTeam = 2;
    led_colorActTeam = teamColorRGB[ actTeam ];
    swapTeam();
  }
  else if ( ir_shooterID > lastPlayerTeam2 && ir_shooterID <= lastPlayerTeam3)
  {
    Serial.println("Team Green ");
    prevTeam = actTeam;
    actTeam = 3;
    led_colorActTeam = teamColorRGB[ actTeam ];
    swapTeam();
  }
  else if ( ir_shooterID > lastPlayerTeam3 && ir_shooterID <= lastPlayerTeam4 )
  {
    Serial.println("Team Purple ");
    prevTeam = actTeam;
    actTeam = 4;
    led_colorActTeam = teamColorRGB[ actTeam ];
    swapTeam();
  }

  if ( actTeam != 0 )
  {
    actTeamPoints = teamScores[actTeam];
  }

  if ( led_ColorWipe == 1)
  {
    led_TeamChange();
    delay (100);                                // clear the strip to allow the points to be displayed
    strip.clear();
    strip.show();
  }
}

//-----------------------------------------------------------------------------------------------------------
void swapTeam()                                 // checks if flag has been hit by player of the same team (previous) or of a new team
{
  if ( actTeam != prevTeam )
  {
    led_ColorWipe = 1;
  }
}

//-----------------------------------------------------------------------------------------------------------
void displayWinner()
{
  winningTeamColor = teamColorRGB[leadingTeam];           // has to be mentioned for client flag
  if ( ( currentTime - counterPreviousTime ) > 1000 )
  {
    strip.clear();
    strip.show();
    delay( 500 );
    if ( leadingScore < maxPoints )                       // if the game ended by any other condition team with the highest points wins
    {
      strip.fill( teamColorRGB[leadingTeam], 0, strip.numPixels() );
    }
    else strip.fill( winningTeamColor, 0, strip.numPixels() );
    strip.show();
    counterPreviousTime = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
void identifyLeadingTeam()
{
  for ( int i = 1; i < sizeof(teamScores) / sizeof(teamScores[1]); i++ )
  {
    if ( teamScores[i] > leadingScore )
    {
      leadingScore = teamScores[i];
      leadingTeam = i;
    }
  }
  if (debug == 1 && leadingScore != 0)
  {
    Serial.print( "Team ");
    Serial.print(teamColor[leadingTeam]);
    Serial.print(" is leading the pack with ");
    Serial.print(leadingScore);
    Serial.println(" points");
  }
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- LED Routines ------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
/* This routine will display the new team color when a color wipe is initiated.
*/
void led_TeamChange()
{
  colorWipe( black, 50 );
  r_colorWipe( led_colorActTeam, 50 );        // colorWipe is only carried out, if a new team hits the flag, not if a player from the owning team does
  led_ColorWipe = 0;                          // marker for the flag being taken by actTeam already
}

//-----------------------------------------------------------------------------------------------------------
/************************************************************************************************************
   Function is still to be developed. Is working for server but not for client
   teamScores already have been parsed.
   Show the the team scores every 3rd time ( = 15 seconds ), of it is bigger than 0.
*/
void displayLEDScore()
{
  c++;
  int teamOrder[] = {1, 2, 3, 4};

  if ( c == 3 )
  {
    for (int n = 0; n < 4; n++)
    {
      for (int i = 0; i < 3 ; i++)           // comparing the score with its neighbour
      {
        if (teamScores[teamOrder[i]] < teamScores[teamOrder[i + 1]])
        {
          int temp = teamOrder[i];
          teamOrder[i] = teamOrder[i + 1];
          teamOrder[i + 1] = temp;
        }
      }
    }

    if ( teamScores[teamOrder[0]] > 0 )
    {
      colorWipe( black, 50 );                 // clear LED strip from showing active team when points of leading team > 0
    }

    for ( int i = 0  ; i < 4 ; i++ )
    {
      if ( teamScores[teamOrder[i]] > 0 )     // just display for teams that have points
      {
        led_Quotient = strip.numPixels() * 100 / maxPoints;
        led_Percent = led_Quotient * teamScores[teamOrder[i]] / 100 ;

        strip.fill( teamColorRGB[teamOrder[i]], ( strip.numPixels() - led_Percent ), led_Percent );
        strip.show();
        delay(200);
      }
    }
    c = 0;
  }
}

//-----------------------------------------------------------------------------------------------------------
// displaying the received signal quality for 20 seconds or if the connection is bad (color = red)
void displaySignalstrength()
{
  if ( WiFi.status() == WL_CONNECTED && flagInPosition == 0 )
  {
    long rssi = WiFi.RSSI();
    // calculation of pixel amount to be displayed
    int led_previousConnectionQuality = led_connectionQuality;
    led_connectionQuality = int( ( 120 - abs( WiFi.RSSI() ) ) / 10 );
    // resetting delay value for colors that are NOT red
    String signalStrength = String(rssi);           // converting number to string

    if ( currentTime - counterPreviousTime >= countIntervall )
    {
      c++;
      //Serial.print( "Connection Quality (1-10):");
      //Serial.print( led_connectionQuality );
      //Serial.print( " (RSSI: " );
      //Serial.print( rssi );
      //Serial.println( ")" );


      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! - c wieder auf 20 ändern
      if ( c > 2 && led_connectionColor != red )  // connection status will be displayed for 20 seconds
      {
        flagInPosition = 1;
        c = 0;
      }
      counterPreviousTime = currentTime;
    }

    /* Assigning colors to the different amounts of pixels indicating
       green = excellent 65280, yellow = good 16776960, orange = poor, red = bad
    */
    if ( led_connectionQuality > 11 ) led_connectionQuality = 11;  // putting a cap on signal quality

    else if ( led_connectionQuality > 6 ) led_connectionColor = green;

    else if ( led_connectionQuality == 6 || led_connectionQuality == 5 ) led_connectionColor = yellow;

    else if ( led_connectionQuality == 4 || led_connectionQuality == 3 ) led_connectionColor = orange;

    else led_connectionColor = red;

    strip.fill( led_connectionColor, strip.numPixels() - led_connectionQuality, 10 );
    strip.show();

    if ( led_connectionColor == red)                               // on a bad connection the remaining LEDs start to flash
    {
      delay( 400 );
      strip.fill( black, 0, strip.numPixels() );
      strip.show();
      delay( 400 );
    }
    if ( led_previousConnectionQuality != led_connectionQuality )  // refreshes LEDs on a change
    {
      strip.clear();
      strip.show();
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// shows the percentage of completion of the maxPoints of a team in 10%-steps by pixel 1 ... 10
void displayLEDProgress()
{
  if ( actTeam == 0 )
  {
    displayAllColors();
  }
  else if ( actTeam > 0 )
  {
    led_Quotient = strip.numPixels() * 100 / maxPoints;
    led_Percent = led_Quotient * actTeamPoints / 100;

    // for network mode just display of active team
    strip.fill( led_colorActTeam, 0, strip.numPixels() );
    strip.show();
  }
}

//-----------------------------------------------------------------------------------------------------------
//------------------------------------------------ WiFi Routines --------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void wifiConnect()                            // Connect to wifi network
{
  // if not connected to Wifi
  if ( WifiStatus != 3 )
  {
    while (wifiMulti.run() != WL_CONNECTED)
    {
      yield();
      colorBand_bg( 1 );                      // while looking for a connection an Amazon Echo-like animation is displayed
    }

    Serial.println('\n');
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());              // Tell us what network we're connected to
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());           // Send the IP address of the ESP8266 to the computer

    Udp.begin( localPort );
    Serial.printf( "UDP listening on port %d\n", localPort );

    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
    led_YES(5);
  }
  WifiStatus = WiFi.status();                   // Constantly update WiFiStatus variable to re-enter WiFi connection when lost
}

//-----------------------------------------------------------------------------------------------------------
// Generic UDP broadcast function which appends a message_prefix and sends out a broadcast
void sendUdpBroadcast(String udp_string)
{
  Udp.beginPacket( broadcast, localPort );

  udp_string = MESSAGE_PREFIX + udp_string;

  for ( int i = 0; i < udp_string.length(); i++ )
  {
    Udp.write( udp_string[i] );
  }
  Udp.endPacket();
  delay(150);       // Without this delay of 100ms the UDP packets are not reliably sent. yield() is also not working.
}

//-----------------------------------------------------------------------------------------------------------
// Send out an update of team (actTeam) owning this flag
void sendFlagUpdate()
{
  Udp.beginPacket( broadcast, localPort );

  String udp_string;
  char buffer[12];

  for ( int i = 0; i < mac_address.length(); i++ )
  {
    udp_string = udp_string + mac_address[i];
  }
  sprintf( buffer, "%012d", millis() );
  udp_string = NETMSG_FLAGALIVE + udp_string + "/" + (String)buffer + "/" + (String)actTeam;
  sendUdpBroadcast(udp_string);
}

//-----------------------------------------------------------------------------------------------------------
// Constantly listen to incoming UDP packages, distinguish packet types by packet size
void udpListener()
{
  // read the packet into packetBuffer
  int packetSize = Udp.parsePacket();
  int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
  packetBuffer[n] = 0;

  String packetBufferString = (String)packetBuffer;

  // If a packet is received
  if ( packetSize != 0)
  {
    if ( debug == 1 ) Serial.println( packetBufferString );

    // If the packet is for SimpleCoil
    if ( packetBufferString.substring(0, 11) == (String)MESSAGE_PREFIX)
    {
      packetBufferString = packetBufferString.substring(11);

      // If the packet is a FLAGSCORE update and this flag is in Domination Client mode
      if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGSCORE && operationMode == 2 )
      {
        packetBufferString = packetBufferString.substring(9);

        parseScore( packetBufferString );
      }

      // If the packet is a TEAMSET - given the last player-ID of each team( TEAMSET16/32/48/63 )
      else if ( packetBufferString.substring(0, 7) == (String)NETMSG_TEAMSET )
      {
        packetBufferString = packetBufferString.substring(7);

        parseTeams( packetBufferString );
      }

      // If the packet is a Game Mode
      else if ( packetBufferString.substring(0, 8) == (String)NETMSG_GAMEMODE && gameMode == 0 )
      {
        packetBufferString = packetBufferString.substring(8);
        parseMode( packetBufferString );
      }

      // If the packet is a STARTGAME
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_STARTGAME )
      {
        packetBufferString = packetBufferString.substring(9);

        led_YES(50);
        if (gameStarted == 0) sendUdpBroadcast("STARTGAME");
        gameStarted = 1;
      }

      // If the packet is an ENDGAME
      else if ( packetBufferString.substring(0, 7) == (String)NETMSG_ENDGAME && gameStarted == 1 )
      {
        packetBufferString = packetBufferString.substring(7);
        
        if ( operationMode =! 42 ) sendUdpBroadcast("ENDGAME");
        operationMode = 42;

      }

      // If the packet is a RESET
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGRESET && operationMode == 42 )
      {
        packetBufferString = packetBufferString.substring(9);

        if ( operationMode = 42 ) sendUdpBroadcast("RESET");
        led_YES(50);
        resetFunction();
      }

      // If the packet is a REPLAY
      else if ( packetBufferString.substring(0, 6) == (String)NETMSG_REPLAY && operationMode == 42 )
      {
        packetBufferString = packetBufferString.substring(9);

        if ( operationMode = 42 ) sendUdpBroadcast("REPLAY");
        led_YES(50);
        replayFunction();
      }

      // If the packet is a SHOWFLAG
      else if ( packetBufferString.substring(0, 8) == (String)NETMSG_SHOWFLAG )
      {
        packetBufferString = packetBufferString.substring(9);

        if ( gameMode == 0 ) baseColor = white;
        else if ( gameMode > 0 ) baseColor = green;
        else baseColor = red;
        r_colorWipe( baseColor, 50);
        delay(1000);
        colorWipe( black, 50);
        delay(1000);
      }

      // If the packet is a FLAGTAKEN
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGTAKEN && gameMode == 5 )
      {
        packetBufferString = packetBufferString.substring(9);
        parseFlagCapture( packetBufferString );
      }

      // If the packet is a FLAGCAPTURED
      else if ( packetBufferString.substring(0, 12) == (String)NETMSG_FLAGCAPTURED && gameMode == 5 && flagTaken == 1)
      {
        packetBufferString = packetBufferString.substring(9);
        resetFlag();
      }

      // If the packet is a FLAGRESTORED
      else if ( packetBufferString.substring(0, 12) == (String)NETMSG_FLAGRESTORED && gameMode == 5 )
      {
        packetBufferString = packetBufferString.substring(9);
        flagOwner = 0;
      }
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for separate team scores
void parseScore( String score )
{
  String score1 = score.substring(0, 3);
  teamScores[1] = score1.toInt();
  String score2 = score.substring(4, 7);
  teamScores[2] = score2.toInt();
  String score3 = score.substring(8, 11);
  teamScores[3] = score3.toInt();
  String score4 = score.substring(12, 15);
  teamScores[4] = score4.toInt();
  String score5 = score.substring(16, 19);
  maxPoints = score5.toInt();

  if ( debug == 1)
  {
    for ( int i = 1; i < 5; i++ )
    {
      if ( teamScores[i] > 0 )
      {
        Serial.print( "Score team " );
        Serial.print( i );
        Serial.print( " " );
        Serial.println( teamScores[i] );
      }
    }
    Serial.print( "max points = " );
    Serial.println( maxPoints );
  }
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for number of players of teams
void parseTeams( String team )
{
  String teams1 = team.substring(0, 2);
  lastPlayerTeam1 = teams1.toInt();
  String teams2 = team.substring(3, 5);
  lastPlayerTeam2 = teams2.toInt();
  String teams3 = team.substring(6, 8);
  lastPlayerTeam3 = teams3.toInt();
  String teams4 = team.substring(9, 11);
  lastPlayerTeam4 = teams4.toInt();


  Serial.print( "Last player Team 1 is ID" );
  Serial.println( lastPlayerTeam1);
  Serial.print( "Last player Team 2 is ID" );
  Serial.println( lastPlayerTeam2);
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for the submitted game mode
void parseMode ( String UdpGameMode )
{
  String gMode = UdpGameMode.substring(0, 1);
  gameMode = gMode.toInt();
  led_YES(50);

  Serial.print( "Game Mode " );
  Serial.println( gameMode );
}

//-----------------------------------------------------------------------------------------------------------
void parseFlagCapture( String capture )
{
  String capture1 = capture.substring(0, 1);
  
  if ( baseTeam == capture1.toInt() )           // check if the flag has been taken by your team
  {
    String capture2 = capture.substring(1, 3);  // only if that is true,
    flagOwner = capture2.toInt();               // assign flagOwner - to be ignored by other teams
  }
}


//-----------------------------------------------------------------------------------------------------------
void updateFlagScore()
{
  actTeamPoints = teamScores[actTeam];
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- IR Routines -------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void printPacketInfo( byte * bits )                          // Decode the meaning of the bits in a packet into human-readable form
{
  byte a = bits[0], b = bits[1], c = bits[2], d = bits[3], e = bits[4], f = bits[5], g = bits[6], h = bits[7], i = bits[8], j = bits[9], k = bits[10], l = bits[11], m = bits[12], n = bits[13], o = bits[14], p = bits[15], q = bits[16], r = bits[17], s = bits[18], t = bits[19], u = bits[20];
  if ( e == 0 && f == 0 && g == 1 && h == 0 && i == 0 && j == 0 && a ^ b ^ c ^ d > 0 && k ^ l ^ m ^ n ^ o ^ p ^ q > 0 )
  {
    Serial.print( "gun shot " );
    Serial.print( shotIndex( bits + 1 ) );
    Serial.print( ", shooterID ");
    Serial.print( shooterIndex( bits + 11 ) );
    Serial.print( ", ");

    if ( gameStarted == 1)
    {

      updateActTeam();                                       // recognition of team-id will be carried out on each detected shot

      if ( gameMode == 2 ) showBase();

      else if ( gameMode == 3 ) shootTheFlag();

      else if ( gameMode == 4 ) takehold();

      else if ( gameMode == 5 ) captureTheFlag();
    }
  }
}
