//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode Take & Hold -------------------------------------------
//-----------------------------------------------------------------------------------------------------------
/* Mechanics of the game: initial color for the flag is rainbow. If the flag is shot by a team, the pixels will
   start to count up until the complete stripe is filled with the team color. From this moment the points will
   count for the team. If a different team shoot at the flag, the pixel will be reduced to zero, but the
   points will continue counting until then.
   When the flag is being shot at by a second team and the change is initiated, as soon as the first team
   shoots at the flag, the change will be cancelled and the flag restored to the first team color.
*/

void gameModeTakeAndHold()
{
  if ( baseTeam == 0 && flagTaken == 0 ) displayAllColors();

  if ( baseTeam == 0 && flagTaken == 1 ) led_countUp();

  if ( baseTeam != 0 && flagTaken == 1 ) led_countDown();



  if ( ( currentTime - counterPreviousTime ) > loop_wait_time )
  {
    identifyLeadingTeam();
    updateFlagScore();                      // Update local flag score for team owning this flag
    sendFlagUpdate();                       // Regular send flag updates as keepalives and ownership change info

    counterPreviousTime = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
void takehold()
{
  if ( baseTeam == 0 && flagTaken == 0 )
  {
    baseColor  = teamColorRGB[actTeam];
    prevTeam = actTeam;                        // actual team is parked until color strip is full
    actTeam = 0;                               // is reset to start counting when color strip is full
    flagTaken = 1;
  }
  if ( baseTeam != 0 && flagTaken == 0 )
  {
    prevTeam = actTeam;                        // actual team is parked until color strip is full
    actTeam = baseTeam;                        // baseTeam is still in charge of the flag until color strip is empty
    flagTaken = 1;
  }
  if ( startCounting == 1 )                    // when flag is being shot at during change it will be recovered
  {
    baseColor  = teamColorRGB[actTeam];
    prevTeam = actTeam;
    baseTeam = 0;
    flagTaken = 1;
    Serial.println("Change interrupted");
  }
}


//-----------------------------------------------------------------------------------------------------------
void led_countUp()
{
  if ( startCounting == 0 )
  {
    c = 9;
    startCounting = 1;
    r_colorWipe(black, 50);
  }

  else if ( startCounting == 1 )
  {
    if ( currentTime - counterPreviousTime2 > 1000 )
    {
      strip.setPixelColor( c, baseColor );
      strip.show();
      Serial.print("counting up pixel " );
      Serial.println(c);

      if ( c == 0 )
      {
        startCounting = 0;                     // when color strip is full, baseTeam is acknowledged and points start counting
        actTeam = prevTeam;
        baseTeam = actTeam;
        flagTaken = 0;
        strip.fill( teamColorRGB[baseTeam], 0, strip.numPixels() );
        strip.show();
      }
      c--;
      counterPreviousTime2 = currentTime;
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
void led_countDown()
{
  if ( startCounting == 0 )
  {
    c = 0;
    startCounting = 1;
    strip.fill( teamColorRGB[baseTeam], 0, strip.numPixels() );
    strip.show();
  }
  else if ( startCounting == 1 )
  {
    if ( currentTime - counterPreviousTime2 > 2000 )
    {
      strip.setPixelColor( c, black );
      strip.show();
      Serial.print("deleting pixel " );
      Serial.println(c);

      if ( c == 9 )
      {
        startCounting = 0;
        baseTeam = 0;
        actTeam = 0;
        baseColor  = teamColorRGB[prevTeam];
      }
      c++;
      counterPreviousTime2 = currentTime;
    }
  }
}
