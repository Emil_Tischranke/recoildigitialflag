//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode Domination --------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void gameModeDomination()
{
  countPoints();
  
  if ( ( currentTime - counterPreviousTime ) > loop_wait_time )
  {
      identifyLeadingTeam();
      sendFlagUpdate();                       // Regular send flag updates as keepalives and ownership change info
      updateFlagScore();                      // Update local flag score for team owning this flag
      displayLEDProgress();                   // Display progress of team owning this flag with LED strips
      displayLEDScore();                      // Display the overall score of attending teams with LED strips

    counterPreviousTime = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
void countPoints()
{
  if ( currentTime - counterPreviousTime2 >= countIntervall )
  {
    if ( actTeam != 0 )
    {
      teamScores[actTeam]++;
      actTeamPoints = teamScores[actTeam];
    }
    
    counterPreviousTime2 = currentTime;
  }
}
