//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode Domination --------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void gameModeDomination()
{
  if ( ( currentTime - counterPreviousTime ) > loop_wait_time )
  {
    if ( operationMode == 3)
    {
      // Server routines
      checkNetworkWinningCondition();         // Check winning condition and end game if condition is met
      incrementScore();                       // Increment all team scores according to current flag ownership
      sendScore();                            // Broadcast current team scores to all flags
      cleanupFlags();                         // Remove flags which have not sent updates in
      serverFlagUpdate();                     // Update server flag locally instead of network communication

      // Server+Client routines
      updateFlagScore();                      // Update local flag score for team owning this flag
      identifyLeadingTeam();                  // Constantly look for the leading team
      displayLEDProgress();                   // Display progress of team owning this flag with LED strips
      displayOLEDScores();                    // Display scores on the OLED display
      displayLEDScore();                      // Display the overall score of attending teams with LED strips
    }

    else if ( operationMode == 2 )
    {
      // Server+Client routines
      sendFlagUpdate();                         // Regular send flag updates as keepalives and ownership change info
      updateFlagScore();                      // Update local flag score for team owning this flag
      identifyLeadingTeam();                  // Constantly look for the leading team
      displayLEDProgress();                   // Display progress of team owning this flag with LED strips
      displayOLEDScores();                    // Display scores on the OLED display
      displayLEDScore();                      // Display the overall score of attending teams with LED strips
    }
    counterPreviousTime = currentTime;
  }
}
