//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode Take & Hold -------------------------------------------
//-----------------------------------------------------------------------------------------------------------
/* Mechanics of the game: initial color for the flag is rainbow. If the flag is shot by a team, the pixels will
   start to count up until the complete stripe is filled with the team color. From this moment the points will
   count for the team. If a different team shoot at the flag, the pixel will be reduced to zero, but the
   points will continue counting until then.
   When the flag is being shot at by a second team and the change is initiated, as soon as the first team
   shoots at the flag, the change will be cancelled and the flag restored to the first team color.
*/

void gameModeCaptureTheFlag()
{
  if ( baseTeam == 0 && flagTaken == 0 ) displayAllColors();

  if ( baseTeam != 0 && flagTaken == 1 ) led_beacon(baseColor, 1000 );

  if ( ( currentTime - counterPreviousTime2 ) > loop_wait_time )
  {
    if ( operationMode == 3)
    {
      // Server routines
      checkNetworkWinningCondition();       // Check winning condition and end game if condition is met
      incrementScore();                     // Increment all team scores according to current flag ownership
      sendScore();                          // Broadcast current team scores to all flags
      cleanupFlags();                       // Remove flags which have not sent updates in
      serverFlagUpdate();                   // Update server flag locally instead of network communication
    }

    identifyLeadingTeam();
    updateFlagScore();                      // Update local flag score for team owning this flag
    sendFlagUpdate();                       // Regular send flag updates as keepalives and ownership change info

    if ( actTeam == 0 && flagTaken == 1 )
    {
      changeTime = changeTime + 5 ;         // adding 5 on each 5-second loop
      if ( changeTime > 60 )
      {
        sendUdpBroadcast( "FLAGRESTORED");  // if time limit passed restore the flag without points for opponent
        actTeam = baseTeam;
        flagTaken = 0;
        flagOwner = 0;
        changeTime = 0;
        r_colorWipe( baseColor, 50 );
      }
    }

    Serial.print( " actTeam " );
    Serial.print( actTeam );
    Serial.print( ", flagTaken " );
    Serial.print( flagTaken );
    Serial.print( ",  flagOwner " );
    Serial.print( flagOwner );
    Serial.print( ",  changeTime " );
    Serial.println( changeTime );

    counterPreviousTime2 = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
void captureTheFlag()
{
  if ( baseTeam == 0 )
  {
    baseTeam = actTeam;
    baseColor  = teamColorRGB[actTeam];
    r_colorWipe( baseColor, 50 );                     // colorWipe carried out, if  flag is hit fist time
  }

  if ( baseTeam != 0 && baseTeam != actTeam && flagTaken == 0 )
  {
    flagTaken = 1;
    int ir_shooterID = shooterIndex( bits + 11 );    // detected shooterID from 1 - 64
    flagOwner = ir_shooterID;
    sendUdpBroadcast( "FLAGTAKEN" + String(actTeam) + String(flagOwner)); // flag is taken BY actTeam and flagOwner
    actTeam = 0;                                     // no more counting of points
    Serial.print( "Flag has been captured by Team ");
    Serial.print( baseTeam );
    Serial.print( " and Player " );
    Serial.println( flagOwner );
  }

  if ( flagTaken == 1 ) actTeam = 0;

  if ( baseTeam == actTeam && flagOwner == shooterIndex( bits + 11 ))
  {
    if ( millis() - startTime < 120000 )
    {
      sendUdpBroadcast( "FLAGCAPTURED");
      sendUdpBroadcast( "ADDPOINTS" + String(actTeam) + "50");
      flagOwner = 0;
      flagTaken = 0;
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
void resetFlag()
{
  actTeam = baseTeam;
  flagTaken = 0;
  flagOwner = 0;
  changeTime = 0;
  r_colorWipe( baseColor, 50 );
}

//-----------------------------------------------------------------------------------------------------------
