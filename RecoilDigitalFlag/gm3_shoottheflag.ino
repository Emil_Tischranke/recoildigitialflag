//-----------------------------------------------------------------------------------------------------------
//--------------------------------------- Game Mode Shoot the Flag ------------------------------------------
//-----------------------------------------------------------------------------------------------------------
/* This program will display random team colours on the flag for a random amount of seconds. The team that
   succeeds to hit the flag when it shows their team colour will directly gain a certain amount of points.
   This will be achieved by the UDP "ADDPOINTS" command followed by the team number and the amount.
   The team will be identified as actTeam by the shot, but actTeam will be cleared directly afterwards not
   to confuse the server with counting owned flags.
*/


void gameModeShootTheFlag()
{
  randomizeColors();

  if ( ( currentTime - counterPreviousTime ) > loop_wait_time )
  {
    identifyLeadingTeam();
    updateFlagScore();                      // Update local flag score for team owning this flag

    counterPreviousTime = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
void randomizeColors()
{
  yield();
  if ( currentTime - counterPreviousTime2 >= countIntervall * changeTime )   // the color is wiped after a random time of seconds
  {
    randomNumber = random( 1, 5 );                                           // the color is chosen at random

    if ( randomNumber == c )                                                 // to prevent the same color coming to often after another
    {
      randomNumber = random( 1, 5 );
    }
    if ( randomNumber == c )
    {
      randomNumber = random( 1, 5 );
    }
    if ( randomNumber == c )
    {
      randomNumber = random( 1, 5 );
    }
    changeTime = random(5, 16);

    c = randomNumber;
    counterPreviousTime2 = currentTime;

    baseColor = teamColorRGB[ randomNumber ];                                // correlation of random numbers to the colors

    colorWipe(black, 100);                                                   // color is wiped with black from top to bottom
    r_colorWipe(baseColor, 100);                                             // and then filled with the new color from bottom to top
    baseHit = 0;                                                             // to allow points to be gaind by shots

    if ( debug == 1 )
    {
      Serial.print("random Number : ");
      Serial.println(randomNumber);
      Serial.print("Change Time : ");
      Serial.println(changeTime);
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
void shootTheFlag()
{
  updateActTeam();
  teamAddPoint( actTeam );

  checkWinningCondition();

  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 0, "Shoot the Flag");
  display.setTextAlignment(TEXT_ALIGN_LEFT);                        // current team and its points are shown on the display
  display.setFont(ArialMT_Plain_10);
  display.drawString(10, 20, "Team");
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(118, 20, "Points");
  display.setFont(ArialMT_Plain_24);
  display.drawString(118, 40, String( actTeamPoints ) );
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(15, 40, String( teamID ));
  display.display();

  if ( debug == 1 )
  {
    Serial.print( "Team: " );
    Serial.print( teamID );
    Serial.print( " has " );
    Serial.print( actTeamPoints );
    Serial.println( " points" );
  }
  actTeam = 0;
}

//-----------------------------------------------------------------------------------------------------------
void teamAddPoint( int team )
{
  if ( gameMode == 3)
  {
    if ( randomNumber == ( team ) && baseHit == 0 )                         // if team has the matching color and shooting at base is allowed
    {
      sendUdpBroadcast( "ADDPOINTS" + String(actTeam) + String(10) );       // 10 points are added to actual team

      baseHit = 1;
      led_YES(50);
    }
    else // if ( randomNumber != 1)                                         // if the colors do not match
    {
      led_NO();
      r_colorWipe(baseColor, 100);
    }
  }
}
