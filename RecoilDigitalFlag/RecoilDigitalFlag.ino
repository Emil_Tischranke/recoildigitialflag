//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Include Libraries ---------------------------------------------
//-----------------------------------------------------------------------------------------------------------

//Flag is only working with ESP8266 board version 2.3.0, later seems not to work
// Adafruit NeoPixel Version 1.8.4 (1.8.5 not working) https://github.com/adafruit/Adafruit_NeoPixel
#include <Adafruit_NeoPixel.h>
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi
#include <ESP8266WiFi.h>
// Wifi 1.2.7 https://www.arduino.cc/en/Reference/WiFi
#include <ESP8266WiFiMulti.h>   // Include the Wi-Fi-Multi library
// https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/ESP8266WiFiMulti.h
#include <WiFiUdp.h>
// OneButton Version 1.5.0 https://github.com/mathertel/OneButton
#include <OneButton.h>
// Driver for the SSD1306 and SH1106 based 128x64, 128x32, 64x48 pixel OLED display running on
// ESP8266/ESP32 Version 4.1.0 https://github.com/ThingPulse/esp8266-oled-ssd1306
#include <Wire.h>
// for OLED SSD1306 I2C 4-pin
#include "SSD1306Wire.h"
// Include a setup file for configuration
#include "setup.h"


//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Global Definitions --------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Definition of global variables - values initialized in setup()
// Definition of global constants including values


//----- Network Definitions -----
const char* ssid     = STASSID;
const char* password = STAPSK;
const unsigned int localPort = 17500;               // local port to listen on - same as in the SimpleCoil app
const String mac_address = WiFi.macAddress();
const IPAddress broadcast(255, 255, 255, 255);      // Broadcast Address
long rssi;
int WifiStatus;                                     // the Wifi radio's status prior to connection
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1];      // buffers for receiving and sending data
WiFiUDP Udp;
ESP8266WiFiMulti wifiMulti;

//-----GLOBAL STRINGS-----
String MESSAGE_PREFIX       = "SimpleCoil:";
String NETMSG_STARTGAME     = "STARTGAME";
String NETMSG_ENDGAME       = "ENDGAME";
String NETMSG_FLAGALIVE     = "FLAGALIVE";
String NETMSG_FLAGSCORE     = "FLAGSCORE";
String NETMSG_FLAGRESET     = "FLAGRESET";          // resetting all parameters
String NETMSG_REPLAY        = "REPLAY";             // just resetting counters - keeping flag modes
String NETMSG_TEAMSET       = "TEAMSET";            // telling the last player-ID for each team
String NETMSG_GAMEMODE      = "GAMEMODE";           // new message for multiple games, setting gameMode over UDP
String NETMSG_ADDPOINTS     = "ADDPOINTS";          // new message e.g. "ADDPOINTS0130" to add 30 points for team 1
String NETMSG_SHOWFLAG      = "SHOWFLAG";           // carries out a white colorwipe on the adressed flag
String NETMSG_FLAGTAKEN     = "FLAGTAKEN";          // flag has been taken by another team
String NETMSG_FLAGRESTORED  = "FLAGRESTORED";       // time has run out and flag has been restored
String NETMSG_FLAGCAPTURED  = "FLAGCAPTURED";       // flag has been booked in by the taking team

//----- OLED Variables -----               // 4-pin I2C SSD 1306 OLED
#define SDA D2                             // SDA - Serial Data
#define SCL D5                             // SCL - Serial Clock
SSD1306Wire  display(0x3C, SDA, SCL);      // Address set here 0x3C is most common

//----- OneButton Definitions -----
#define PIN_BUTTON D7
OneButton button(PIN_BUTTON, true);
typedef enum { ACTION_NONE, ACTION_SINGLE, ACTION_DOUBLE, ACTION_LONGSTART, ACTION_PRESS, ACTION_LONGSTOP } MyActions;
MyActions nextAction;

//----- IR Definitions -----
const int IR_PIN = D6;                     // The pin to which the active-low IR sensor is attached
const unsigned long MAX_PULSELENGTH = 200;
const int MAX_PULSES = 100;                // Maximum number of pulses that may be detected before printed to serial
const int MAX_MESSAGE_BITS = 100;          // Maximum number of bits per message
const bool PRINT_INVALID_PACKETS = true;   // True to print debugging information about invalid packets, false to skip them.
const int END_OF_PACKET = -1;              // Instead of a time interval, write this value to dts to indicate the end of a packet.
int dts[MAX_PULSES];                       // Microsecond deltas detected between edge transitions
volatile int dtIndex = 0;
byte bits[MAX_MESSAGE_BITS];               // F-2F decoded message bits
byte lastBits[MAX_MESSAGE_BITS];           // F-2F decoded message bits

//----- LED Definitions -----
#define LED_PIN   D1
#define LED_COUNT 10
Adafruit_NeoPixel strip( LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800 );

int LED_BRIGHTNESS = 50;
int led_connectionQuality = int( ( 120 - abs( WiFi.RSSI() ) ) / 10 ); // calculation of number of pixels correlating to signal strength
int led_connectionColor = 0;                                          // connection color
int c = 0;                                                            // counter

// LED color declarations
const unsigned int black =     strip.Color(   0,   0,   0 );
const unsigned int blue =      strip.Color(   0,   0, 255 );
const unsigned int red =       strip.Color( 255,   0,   0 );
const unsigned int green =     strip.Color(   0, 255,   0 );
const unsigned int purple =    strip.Color( 255,   0, 255 );
const unsigned int white =     strip.Color( 255, 255, 255 );
const unsigned int cyan =      strip.Color(  18, 205, 242 );
const unsigned int orange =    strip.Color( 255,  50,   2 );
const unsigned int yellow =    strip.Color( 255, 255,   0 );
const unsigned int lightblue = strip.Color(   0, 150,  80 );
const unsigned int led_colorMenuItem1 = blue;              // color menu-item 1 - blue
const unsigned int led_colorMenuItem2 = green;             // color menu-item 3 - green
const unsigned int led_colorMenuItem3 = red;               // color menu-item 2 - red
int led_currentMenuColor;                                  // current color of the main menu
int led_currentMenu2Color;                                 // current color of the game menu

// ----- Team declarations -----
const char *teamColor[] = {"None", "Red", "Blue", "Green", "Purple"};
const int teamColorRGB[] = {black, red, blue, green, purple};

// number of players in team
// default values are:
unsigned int lastPlayerTeam[] = {0, 16, 32, 48, 63};

//----- Timing Definitions -----
const unsigned int loop_wait_time = 5000;                 // 5 seconds
const unsigned int keepalive_wait_time = 10000;           // 10 seconds
const unsigned long countIntervall = 1000;                // intervall in ms to count points
unsigned long counterPreviousTime;
unsigned long counterPreviousTime2;
unsigned long currentTime;

//----- Game Definitions -----
// We have lots of overlaps here, i.e. the active team is the same as the baseTeam
int operationMode = 0;                                 // 0 = main menu, 1 = standalone, 2 = Client, 3 = Server, 42 = Display Winner
int server = 0;
int gameMode = 0;                                      // 0 = game menu, 1 = Domination, 2 = Base Attack, 3 = Shoot the Flag, 4 = Take & Hold
const char *gameModeDes[] = {"MENU", "DOMINATION", "BASE ATTACK", "SHOOT THE FLAG", "TAKE & HOLD"};
int gameStarted;                                       // variable that indicates game has been started by button or by UDP message
int menuInit;                                          // places the selector in the top line when the menu is entered first time
int menuInput = 0;                                     // indicator that shows operation of the flag to prevent automatic start

int leadingScore;                                      // highest score during the game
int leadingTeam;                                       // team leading the game currently
int actTeam;                                           // last registered shot
int prevTeam;                                          // team that held the flag prior to a detected shot - can be the same team as actTeam
int winningTeamColor;
int led_ColorWipe;                                     // indicates that a ColorWipe has to be carried out
int baseColor;
int baseTeam;                                          // team that owns the base
int baseHit;                                           // activated if the base it hit by a hostile team
int baseHeal;                                          // activated if the base it hit by the own broadcastteam
int flagTaken;                                         // is set when the flag has been take and is no longer idle mode
int teamID;
int ir_shooterID;
int led_colorActTeam;
int gunShot;                                           // gun shot detected
int flagInPosition = 0;                                // indicates that the flag is in position for game in WiFi mode
int randomNumber;
int changeTime;
int playTime;                                          // play time in minutes
unsigned long playTimeInMillis;                        // conversion of minutes into millis
unsigned long startTime;                               // begin of the game
int startCounting;

int maxPoints;                                         // limit for the maximum points, whoever reaches these points first, wins the game
int actTeamPoints;
int led_Quotient;
int led_Percent;
int flagOwner;

float maxHitPoints;
int actHitPoints;

unsigned int teamScores[5];
struct Flag
{
  String mac;
  String zeitpunkt;
  String team;
};
const unsigned int MAX_FLAGS = 8;
Flag flags[MAX_FLAGS];


//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------Setup--------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Initialize global variables - this should be done here so a reset can be triggered via resetFunction calling setup()
void setup()
{
  Serial.begin( 115200 );                                  // Initialize Serial Console
  pinMode( LED_BUILTIN, OUTPUT );
  digitalWrite( LED_BUILTIN, HIGH );

  //----- Initialize OLED -----
  Wire.begin( SDA, SCL );
  display.init();
  display.flipScreenVertically();                          // turns the screen so that the contacs are above
  oledStartscreen();
  Serial.println( "OLED initialized." );

  //----- Initialize LED -----
  strip.begin();                                           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.clear();                                           // turn OFF all pixels
  strip.show();                                            // update all pixels
  strip.setBrightness( LED_BRIGHTNESS );                   // Set BRIGHTNESS to about 1/5 (max = 255)
  led_currentMenuColor = blue;
  led_currentMenu2Color = blue;
  Serial.println( "LED Strips initialized." );

  //----- Initialize IR -----
  pinMode( IR_PIN, INPUT );
  Serial.println( "Infrared Sensor initialized." );

  //----- Initialize OneButton -----
  nextAction = ACTION_NONE;
  button.setPressTicks( 1000 );
  button.setClickTicks( 600 );
  button.setDebounceTicks( 50 );
  button.attachClick( singleclick );
  button.attachDoubleClick( doubleclick );
  button.attachLongPressStart( longPressStart );
  // button.attachLongPressStop(longPressStop); not actively used yet
  // button.attachDuringLongPress(longPress); not actively used yet
  Serial.println( "Button initialized." );

  // ----- Initialize WiFi -----
  // This is important because it clears still active entries on access points preventing reconnections
  WifiStatus = 0;
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  rssi = WiFi.RSSI();
  // standard WiFi networks - add additional Wi-Fi networks you want to connect to
  wifiMulti.addAP("Recoil Game Hub_WDS", "recoil123");
  wifiMulti.addAP("Recoil Game Hub", "recoil123");
  wifiMulti.addAP("Moria", ".Mellon.");
  Serial.println( "WiFi initialized." );

  //----- Initialize timing variables -----
  counterPreviousTime = 0;
  counterPreviousTime2 = 0;
  currentTime = millis();

  // ----- Initialize game variables -----
  operationMode = 0;                                     // persistent variable
  gameMode = 0;                                          // persistent variable
  gameStarted = 0;
  menuInit = 0;

  leadingScore = 0;
  leadingTeam = 0;
  actTeam = 0;
  prevTeam = 0;                                          // team that held the flag prior to a detected shot - can be the same team as actTeam
  winningTeamColor = 0;
  baseColor = 0;
  baseTeam = 0;
  baseHit = 0;
  baseHeal = 0;
  flagTaken = 0;
  teamID = 0;
  led_colorActTeam = 0;
  gunShot = 0;                                          // gun shot detected
  flagOwner = 0;
  changeTime = 0;

  playTime = 10;                                        // play time in minutes
  playTimeInMillis = (playTime * 60 * 1000);            // conversion of minutes into millis
  startTime = 0;                                        // begin of the game
  startCounting = 0;

  maxPoints = 300;                                      // still defined for older games, should be variable in the future
  actTeamPoints = 0;
  led_Quotient = 0;                                     // necessary here for older games, dynamically calculated in network mode; strip.numPixels() / maxPoints
  led_Percent = 0;                                      // necessary here for older games; led_Quotient * actTeamPoints

  maxHitPoints = 10;
  actHitPoints = maxHitPoints;

  for ( int counter = 0; counter < 8; counter++ )       // debug for teams 1-8
  {
    flags[counter].mac = "";
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
    teamScores[counter] = 000;
  }
  Serial.println( "Game initialized." );
  rainbow( 3 );                                         // illumination at start of program
  display.clear();
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- Main Loop ---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Game logic should be implemented on top logic main loop instead of subroutines

void loop()
{
  button.tick();                               // keep watching the push button
  readIR();                                    // keep listening to the IR sensor
  currentTime = millis();                      // keep currentTime up-to-date

  if ( nextAction == ACTION_LONGSTART ) resetFunction(); // reset with longpress

  if ( operationMode == 0 )  mainMenu();       // main menu

  //-------------------------------------------- Standalone Mode ---------------------------------------------
  else if ( operationMode == 1 )
  {
    if ( gameMode == 0)gameMenu( 2, "Standalone Game", 1 );   // game menu stand-alone

    if ( gameMode == 1 ) countPoints();                       // Domination

    else if ( gameMode == 2 ) countHitPoints();               // Base Attack

    else if ( gameMode == 3 ) randomizeColors();              // Shoot the Flag

    else if ( gameMode == 4 ) gameModeTakeAndHold();          // Take & Hold
  }

  //-----------------------------------------------------------------------------------------------------------
  //---------------------------------------------- Network Modes ----------------------------------------------
  /************************************************************************************************************
      In network modes the flags will try to connect to the network hub. After successful connection the RSSI
      ( connection quality ) will be displayed as a number in the display and as amount of LEDs in colors.
      When the flag is located in a suitable position (at least orange color) it is put into ready-mode with a
      button click. It will then send out a green beacon signal every 3 seconds. The game can now be started
      from any flag with a double click. All flags will then send out a blue beacon signal every second until
      they have been taken. Then they will show the color of the active team and the points will start counting.
  ************************************************************************************************************/
  //-------------------------------------------- Domination Client --------------------------------------------
  else if ( operationMode == 2 )
  {
    wifiConnect();
    udpListener();                              // listen to incoming UDP, process input and reply

    if ( flagInPosition == 0 ) displaySignalstrength();

    else if ( flagInPosition == 1)
    {
      if ( gameStarted == 0 )
      {
        if ( gameMode == 0 )
        {
          led_beacon(blue, 3000);
          display.setTextAlignment(TEXT_ALIGN_CENTER);
          display.setFont(ArialMT_Plain_16);
          display.drawString( 63, 0, "FLAG READY" );
          display.drawString( 63, 24, "waiting for" );
          display.drawString( 63, 44, "game mode" );
          display.display();
        }
        else flagReady();

        if ( ( currentTime - counterPreviousTime2 ) > loop_wait_time )
        {
          sendFlagUpdate();                     // Regular send flag updates as keepalives and ownership change info
          counterPreviousTime2 = currentTime;
        }
      }

      if ( gameStarted == 1 )
      {
        if ( gameMode == 1 ) gameModeDomination();
        if ( gameMode == 2 ) gameModeBaseAttack();
        if ( gameMode == 3 ) gameModeShootTheFlag();
        if ( gameMode == 4 ) gameModeTakeAndHold();
        if ( gameMode == 5 ) gameModeCaptureTheFlag();
      }
    }
  }

  //-------------------------------------------- Domination Server --------------------------------------------
  else if ( operationMode == 3 )
  {
    if ( maxPoints == 0) chooseDurationMenu( 2, "Duration", 1 ); // game menu choose duration

    else
    {
      wifiConnect();
      udpListener();                              // listen to incoming UDP, process input and reply

      if ( flagInPosition == 0 ) displaySignalstrength();

      else if ( flagInPosition == 1 )
      {
        if ( gameStarted == 0 )
        {
          if ( gameMode == 0 ) led_beacon(blue, 3000);

          else flagReady();

          if ( ( currentTime - counterPreviousTime2 ) > loop_wait_time )
          {
            sendFlagUpdate();                     // Regular send flag updates as keepalives and ownership change info
            counterPreviousTime2 = currentTime;
          }
        }

        if ( gameStarted == 1 )
        {
          if ( gameMode == 1 ) gameModeDomination();
          if ( gameMode == 2 ) gameModeBaseAttack();
          if ( gameMode == 3 ) gameModeShootTheFlag();
          if ( gameMode == 4 ) gameModeTakeAndHold();
          if ( gameMode == 5 ) gameModeCaptureTheFlag();
        }
      }
    }
  }

  //--------------------------------------------- End of game ----------------------------------------------
  else if ( operationMode == 42 )
  {
    if ( leadingScore == 0 && gameStarted == 1 ) led_beacon( red, 500 );

    else displayWinner();

    udpListener();
  }
}

//-----------------------------------------------------------------------------------------------------------
//---------------------------------------------- Subroutines ------------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void resetFunction()
{
  Serial.println("resetting...");
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_24);
  display.drawString(63, 14, "RESET");
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 44, "deleting counters");
  display.display();
  display.clear();
  menuInput = 1;
  setup();
}

void replayFunction() // resetting counters
{
  operationMode = 2;
  gameStarted = 0;
  leadingScore = 0;
  leadingTeam = 0;
  actTeam = 0;
  prevTeam = 0;
  baseColor = 0;
  baseTeam = 0;
  teamID = 0;
  actTeamPoints = 0;
  flagOwner = 0;
  flagTaken = 0;
  changeTime = 0;
  counterPreviousTime = 0;
  counterPreviousTime2 = 0;
  for ( int counter = 0; counter < 8; counter++ )       // debug for teams 1-4
  {
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
    teamScores[counter] = 000;
  }
  display.clear();
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
/* Puts the flag into a ready-state where it flashes green every 3 seconds. It then waits for a button push
   or a UDP command from another flag to start the game. If the button push is carried out on the local flag,
   it will as well send out a UDP command to all other flags to start the game. */

void flagReady()
{
  if ( gameMode == 0 ) led_beacon(blue, 3000);

  else
  {
    led_beacon(green, 3000);
    if ( menuInit == 0 )
    {
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      display.setFont(ArialMT_Plain_16);
      display.drawString( 63, 0, "FLAG READY" );
      menuInit = 1;
    }
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);

    display.drawString( 63, 24, "double click" );
    display.drawString( 63, 44, "to start game" );
    display.display();

    if ( nextAction == ACTION_DOUBLE )
    {
      oled_OK();
      led_YES(50);
      gameStarted = 1;
      menuInit = 0;
      display.clear();
      display.display();
      Serial.println( "Game started." );
      sendUdpBroadcast( NETMSG_STARTGAME );

      nextAction = ACTION_NONE;
    }
  }
}


//-----------------------------------------------------------------------------------------------------------
//------------------------------------------ OneButton Routines ---------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void singleclick()
{
  nextAction = ACTION_SINGLE;
}

//-----------------------------------------------------------------------------------------------------------
void doubleclick()
{
  nextAction = ACTION_DOUBLE;
}

//-----------------------------------------------------------------------------------------------------------
void longPressStart()
{
  nextAction = ACTION_LONGSTART;
}

//-----------------------------------------------------------------------------------------------------------
//---------------------------------------------- Gamemode: Domination ----------------------------------------
//-----------------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------------
void countPoints()
{
  if ( currentTime - counterPreviousTime >= countIntervall )
  {
    if ( actTeam != 0 )
    {
      teamScores[actTeam]++;
      actTeamPoints = teamScores[actTeam];
    }
    displayLEDProgress();
    checkWinningCondition();
    counterPreviousTime = currentTime;
    displayOLEDScores();
  }
}

//-----------------------------------------------------------------------------------------------------------
void displayOLEDScores()
{
  if ( currentTime - counterPreviousTime2 >= countIntervall )
  {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 0, gameModeDes[gameMode]);
    display.display();

    display.setFont(ArialMT_Plain_10);

    if ( teamScores[1] > 0 )
    {
      display.setTextAlignment(TEXT_ALIGN_LEFT);
      display.drawString(10, 24, "1 Team Red");
      display.setTextAlignment(TEXT_ALIGN_RIGHT);
      display.drawString(120, 24, String(teamScores[1]));
    }
  }
  if ( teamScores[2] > 0 )
  {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 34, "2 Team Blue");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(120, 34, String(teamScores[2]));
  }
  if ( teamScores[3] > 0 )
  {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 44, "3 Team Green");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(120, 44, String(teamScores[3]));
  }
  if ( teamScores[4] > 0 )
  {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 54, "4 Team Purple");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(120, 54, String(teamScores[4]));
  }
  display.display();
  counterPreviousTime2 = currentTime;


  if (debug == 1)
  {
    printGameResults();
  }
}

//-----------------------------------------------------------------------------------------------------------
// shows the percentage of completion of the maxPoints of a team in 10%-steps by pixel 1 ... 10
void displayLEDProgress()
{
  if ( actTeam == 0 )
  {
    displayAllColors();
  }
  else if ( actTeam > 0 )
  {
    led_Quotient = strip.numPixels() * 100 / maxPoints;
    led_Percent = led_Quotient * actTeamPoints / 100;

    // only stand alone mode
    if ( operationMode == 1 )
    {
      if ( led_Percent == 0)                          // activates 1st pixel to show team color, even even if percentage of completion is <10%
      {
        strip.fill( led_colorActTeam, (strip.numPixels() - 1), 1 ); // -1
        strip.show();
      }
      strip.fill( led_colorActTeam, ( strip.numPixels() - led_Percent ), led_Percent );
      strip.show();
    }

    else
    {
      // for network mode just display of active team
      strip.fill( led_colorActTeam, 0, strip.numPixels() );
      strip.show();
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
void printGameResults()
{
  Serial.println();
  Serial.print( "Score Team " );
  Serial.print( actTeam );
  Serial.print( ": " );
  Serial.print( actTeamPoints );
  Serial.println();
  Serial.print( "LED Quotient = " );
  Serial.print(strip.numPixels());
  Serial.print( " *100 / " );
  Serial.print(maxPoints);
  Serial.print( " = " );
  Serial.println(led_Quotient);
  Serial.print( "LED Percent = " );
  Serial.print(led_Quotient);
  Serial.print( " x " );
  Serial.print(actTeamPoints);
  Serial.print( " / 100 = " );
  Serial.println(led_Percent);
}

//-----------------------------------------------------------------------------------------------------------
void checkWinningCondition()
{
  if ( actTeamPoints >= maxPoints )
  {
    winningTeamColor = led_colorActTeam;
    updateActTeam();                                         // update display for a last time to show points
    operationMode = 42;                                      // 42 = Endgame Mode
    sendUdpBroadcast( NETMSG_ENDGAME );

    Serial.print("Team ");
    Serial.print(teamColor[leadingTeam]);
    Serial.print(" has achieved ");
    Serial.print(leadingScore);
    Serial.println(" points!");

    if ( debug == 1 )
    {
      for ( int i = 1; i < 5; i++ )                           // debug for teams 1-4
      {
        if (teamScores[i] > 0)
        {
          Serial.println();
          Serial.print( "Score Team " + (String)(i) );
          Serial.print( ": " );
          Serial.print( teamScores[i] );
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
void checkNetworkWinningCondition()
{
  if (leadingScore >= maxPoints)
  {
    winningTeamColor = teamColorRGB[leadingTeam];
    updateActTeam();                                       // update display for a last time to show points
    operationMode = 42;                                    // 42 = Endgame Mode
    sendUdpBroadcast( NETMSG_ENDGAME );

    Serial.print("Team ");
    Serial.print(teamColor[leadingTeam]);
    Serial.print(" has achieved ");
    Serial.print(leadingScore);
    Serial.println(" points!");
  }
}

//-----------------------------------------------------------------------------------------------------------
void displayWinner()
{
  winningTeamColor = teamColorRGB[leadingTeam];           // has to be mentioned for client flag
  if ( ( currentTime - counterPreviousTime ) > 1000 )
  {
    strip.clear();
    strip.show();
    delay( 500 );
    if ( leadingScore < maxPoints )
    {
      strip.fill( teamColorRGB[leadingTeam], 0, strip.numPixels() );
    }
    else strip.fill( winningTeamColor, 0, strip.numPixels() );
    strip.show();
    counterPreviousTime = currentTime;

    displayOLEDScores();
  }
}

//-----------------------------------------------------------------------------------------------------------
void identifyLeadingTeam()
{
  for ( int i = 1; i < sizeof(teamScores) / sizeof(teamScores[1]); i++ )
  {
    if ( teamScores[i] > leadingScore )
    {
      leadingScore = teamScores[i];
      leadingTeam = i;
    }
  }
  if (leadingScore != 0)
  {
    Serial.print( "Team ");
    Serial.print(teamColor[leadingTeam]);
    Serial.print(" is leading the pack with ");
    Serial.print(leadingScore);
    Serial.println(" points");
  }
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- Game Mode Base Attack ---------------------------------------
//-----------------------------------------------------------------------------------------------------------

void showBase()
{
  if ( ir_shooterID != 0 && baseTeam == 0 )
  {
    baseTeam = actTeam;
    baseColor = led_colorActTeam;
    r_colorWipe( baseColor, 50 );                  // colorWipe carried out, if  flag is hit fist time
  }
  else if ( ir_shooterID != 0 && baseTeam != actTeam)
  {
    baseHit = 1;
  }
  else if (ir_shooterID != 0 && baseTeam == actTeam)
  {
    baseHeal = 1;
  }
}

//-----------------------------------------------------------------------------------------------------------
void countHitPoints()
{
  if ( currentTime - counterPreviousTime >= countIntervall)
  {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 0, gameModeDes[ gameMode ]);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(10, 20, "Health");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(118, 20, "Minutes");
    display.display();
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.setFont(ArialMT_Plain_24);
    display.drawString(118, 40, String( ( playTime  - (currentTime - startTime) / 60000 )) ); // remaining time
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 40, String(actHitPoints));
    display.display();

    if ( baseHeal == 1 && actHitPoints < maxHitPoints )         // no heal possible when hit points (health of base) are at max
    {
      actHitPoints++;
      baseHeal = 0;
      led_Heal();
      displayBaseHealth();
    }
    else if (baseHit == 1)
    {
      actHitPoints--;
      baseHit = 0;
      led_Hit();
      displayBaseHealth();
    }
    if ( debug == 1 )
    {
      Serial.print("remaining Hit Points : ");
      Serial.println(actHitPoints);
    }

    counterPreviousTime = currentTime;
  }
  else if ( actHitPoints <= 0 )                                 // team that removes the last hitpoint conquers the flag
  {
    winningTeamColor = led_colorActTeam;
    operationMode = 42;
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 14, "BASE");
    display.drawString(63, 38, "TAKEN");
    display. display();
    displayWinner();
  }
  else if ( currentTime - startTime >= playTimeInMillis )       // if time has run out and flag has not been taken the owning team wins the flag (before: currentTime - startTime >= playTimeInMillis )
  {
    winningTeamColor =  baseColor;
    operationMode = 42;
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 14, "BASE");
    display.drawString(63, 38, "DEFENDED");
    display. display();
    displayWinner();
  }
}

//-----------------------------------------------------------------------------------------------------------
void displayBaseHealth()
{
  if ( baseTeam != 0 )
  {
    colorWipe( black, 50 );
    strip.fill( baseColor, (strip.numPixels() - actHitPoints), actHitPoints);
    strip.show();
  }
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- LED Routines ------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void displaySignalstrength()                        // displaying the received signal quality when flag not positioned
{
  if ( WiFi.status() == WL_CONNECTED && flagInPosition == 0 )
  {
    long rssi = WiFi.RSSI();
    // calculation of pixel amount to be displayed
    int led_previousConnectionQuality = led_connectionQuality;
    led_connectionQuality = int( ( 140 - abs( WiFi.RSSI() ) ) / 10 );
    // resetting delay value for colors that are NOT red
    String signalStrength = String(rssi);           // converting number to string

    if ( currentTime - counterPreviousTime >= countIntervall * 2 )
    {
      display.clear();
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      display.setFont(ArialMT_Plain_10);
      display.drawString(63, 0, WiFi.SSID());
      display.setFont(ArialMT_Plain_16);
      display.drawString(45, 22, signalStrength);
      display.drawString(81, 22, "dBM");
      display.setFont(ArialMT_Plain_10);
      display.drawString(63, 45, "place flag and press button");
      display.display();

      Serial.print( "Connection Quality (1-10):");
      Serial.print( led_connectionQuality );
      Serial.print( " (RSSI: " );
      Serial.print( rssi );
      Serial.println( ")" );

      counterPreviousTime = currentTime;
    }

    /* Assigning colors to the different amounts of pixels indicating
       green = excellent 65280, yellow = good 16776960, orange = poor, red = bad
    */
    if ( led_connectionQuality > 11 )
    {
      led_connectionQuality = 11;                   // putting a cap on signal quality
    }
    else if ( led_connectionQuality > 6 )
    {
      led_connectionColor = green;
    }
    else if ( led_connectionQuality == 6 || led_connectionQuality == 5 )
    {
      led_connectionColor = yellow;
    }
    else if ( led_connectionQuality == 4 || led_connectionQuality == 3 )
    {
      led_connectionColor = orange;
    }
    else
    {
      led_connectionColor = red;
    }
    strip.fill( led_connectionColor, strip.numPixels() - led_connectionQuality, 10 );
    strip.show();

    if ( led_connectionColor == red)                   // on a bad connection the remaining LEDs start to flash
    {
      delay( 400 );
      strip.fill( black, 0, strip.numPixels() );
      strip.show();
      delay( 400 );
    }
    if ( led_previousConnectionQuality != led_connectionQuality )  // refreshes LEDs on a change
    {
      strip.clear();
      strip.show();
    }

    if ( nextAction == ACTION_SINGLE )
    {
      oled_OK();
      led_YES(50);
      flagInPosition = 1;
      menuInit = 0;
      display.clear();
      display.display();
      nextAction == ACTION_NONE;
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
/************************************************************************************************************
   Show the the team scores every 3rd time ( = 15 seconds ), of it is bigger than 0.
*/
void displayLEDScore()
{
  c++;
  int teamOrder[] = {1, 2, 3, 4};

  if ( c == 3 )
  {
    for (int n = 0; n < 4; n++)
    {
      for (int i = 0; i < 3 ; i++)           // comparing the score with its neighbour
      {
        if (teamScores[teamOrder[i]] < teamScores[teamOrder[i + 1]])
        {
          int temp = teamOrder[i];
          teamOrder[i] = teamOrder[i + 1];
          teamOrder[i + 1] = temp;
        }
      }
    }

    if ( teamScores[teamOrder[0]] > 0 )
    {
      colorWipe( black, 50 );                   // clear LED strip from showing active team when points of leading team > 0
    }

    for ( int i = 0  ; i < 4 ; i++ )
    {
      if ( teamScores[teamOrder[i]] > 0 )       // just display for teams that have points
      {
        led_Quotient = strip.numPixels() * 100 / maxPoints;
        led_Percent = led_Quotient * teamScores[teamOrder[i]] / 100 ;

        strip.fill( teamColorRGB[teamOrder[i]], ( strip.numPixels() - led_Percent ), led_Percent );
        strip.show();
        delay(200);
      }
    }

    // print team order on serial monitor
    for ( int i = 0 ; i < 4 ; i++)
    {
      if ( teamScores[teamOrder[i]] > 0 )       // just display for teams that have points
      {
        Serial.print( "Team " );
        Serial.print( teamOrder[i] );
        Serial.print( " : " );
        Serial.print( teamScores[teamOrder[i]] );
        Serial.println( " points" );
      }
    }
    Serial.print( maxPoints );
    Serial.println( " = max points" );
    c = 0;
  }
}

//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode ClientServer Domination -------------------------------
//-----------------------------------------------------------------------------------------------------------
/* Introduction

  The game is client-server based, the flags normally start as clients, one flag can be defined as server.
  All flags send team changes as broadcasts. The server is listening to these packages and updates the states of all flags constantly.
  The server also regularly updates the teamScores and broadcasts the teamScores to all flags to update the flag colours.
  Flags which do not send a keepalive will be removed from the Flag struct.

  //-----------------------------------------------------------------------------------------------------------
  Game Data

  Two important game data variables are being maintained in this game.
  _________________________________________________________
  struct Flag
  {
  String mac;
  String zeitpunkt;
  String team;
  }

  The struct Flag contains all currently active Flags with the latest timestamp when they received an update.
  The variable starts with empty values and is subsequently filled and maintained.
  The mac address is being used as a unique identifier. Unknown macs are being added to the end of the struct
  Flags send event driven updates when their team changes, this updates the entry in the Flag struct.

  The transmitted strings via UDP have the following format: MAC/MILLISECONDS/TEAM.
  Example: 12:F4:8D:E3:47:03/000000058234/1
  mac: 17 characters
  zeit: 12 characters
  team: 1 character
  _________________________________________________________
  unsigned int teamScores[9]

  The variable teamScores contains all scores of all teams. A Time-phased event is checking which team owns which flag via incrementScore
  Every time incrementScore is executed every team receives points for every flag they currently own in the Flag construct.
  The countable teamScores go from 1-8. teamScore[0] is to be neglected as team 0 is "No team".

  //-----------------------------------------------------------------------------------------------------------
  Subroutine Overview

  udpListener()       Ongoing       Constantly listen to incoming UDP packages, distinguish packet types by packet size
  saveToFlag()        Event         through Flag changes, updating flags with changes
  incrementScore()    Time-phased   Incrementing team scores for each flag owned (loop_wait_time)
  sendScore()         Time-phased   Central server sending out updated score to all flags (loop_wait_time)
*/

//-----------------------------------------------------------------------------------------------------------
// Connect to wifi network
void wifiConnect()
{
  // if not connected to Wifi
  if ( WifiStatus != 3 )
  {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 10, "Connecting to");
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 34, "WiFi");
    display.display();
    digitalWrite(LED_BUILTIN, HIGH);
    strip.show();

    while (wifiMulti.run() != WL_CONNECTED)
    {
      yield();
      Serial.print(".");
      colorBand_bg( 1 );                      // while looking for a connection an Amazon Echo-like animation is displayed
    }
    Serial.println('\n');
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());              // Tell us what network we're connected to
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());           // Send the IP address of the ESP8266 to the computer

    //    Serial.println( "" );
    //    Serial.print( "WiFi connected with IP address " );
    //    Serial.println( WiFi.localIP() );

    Udp.begin( localPort );
    Serial.printf( "UDP listening on port %d\n", localPort );

    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 10, "connection");
    display.drawString(63, 30, "complete");
    display.display();

    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
    display.clear();
  }
  WifiStatus = WiFi.status();                   // Constantly update WiFiStatus variable to re-enter WiFi connection when lost
}

//-----------------------------------------------------------------------------------------------------------
// Generic UDP broadcast function which appends a message_prefix and sends out a broadcast
void sendUdpBroadcast(String udp_string)
{
  Udp.beginPacket( broadcast, localPort );

  udp_string = MESSAGE_PREFIX + udp_string;

  for ( int i = 0; i < udp_string.length(); i++ )
  {
    Udp.write( udp_string[i] );
  }
  Udp.endPacket();
  delay(150);       // Without this delay of 100ms the UDP packets are not reliably sent. yield() is also not working.
}

//-----------------------------------------------------------------------------------------------------------
// Send out an update of team (actTeam) owning this flag
void sendFlagUpdate()
{
  Udp.beginPacket( broadcast, localPort );

  String udp_string;
  char buffer[12];

  for ( int i = 0; i < mac_address.length(); i++ )
  {
    udp_string = udp_string + mac_address[i];
  }
  sprintf( buffer, "%012d", millis() );
  udp_string = NETMSG_FLAGALIVE + udp_string + "/" + (String)buffer + "/" + (String)actTeam;
  sendUdpBroadcast(udp_string);
}

//-----------------------------------------------------------------------------------------------------------
// Update server flag
void serverFlagUpdate()
{
  String udp_string;
  char buffer[12];

  for ( int i = 0; i < mac_address.length(); i++ )
  {
    udp_string = udp_string + mac_address[i];
  }
  sprintf( buffer, "%012d", millis() );
  udp_string = udp_string + "/" + (String)buffer + "/" + (String)actTeam;

  saveToFlag( udp_string );

  if ( debug == 1)
  {
    Serial.println("Flag update sent to local flag (" + udp_string + ")" );
  }
}

//-----------------------------------------------------------------------------------------------------------
// Constantly listen to incoming UDP packages, distinguish packet types by packet size
void udpListener()
{
  // read the packet into packetBuffer
  int packetSize = Udp.parsePacket();
  int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
  packetBuffer[n] = 0;

  String packetBufferString = (String)packetBuffer;

  // If a packet is received
  if ( packetSize != 0)
  {
    // If the packet is for SimpleCoil
    if ( packetBufferString.substring(0, 11) == (String)MESSAGE_PREFIX)
    {
      packetBufferString = packetBufferString.substring(11);

      if ( debug == 1 ) Serial.println( packetBufferString );

      // If the packet is a flag keepalive and this flag is in Domination Server mode
      if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGALIVE && operationMode == 3)
      {
        packetBufferString = packetBufferString.substring(9);
        saveToFlag( packetBufferString );
      }

      // If the packet is a score update and this flag is in Domination Client mode
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGSCORE && operationMode == 2 )
      {
        packetBufferString = packetBufferString.substring(9);

        parseScore( packetBufferString );
        displayOLEDScores();                                        // update points on display of clients directly
      }

      // If the packet is a points update and this flag is in Domination Server mode
      if ( packetBufferString.substring(0, 9) == (String)NETMSG_ADDPOINTS && operationMode == 3)
      {
        packetBufferString = packetBufferString.substring(9);
        parsePoints( packetBufferString );
        displayOLEDScores();                                        // update points on display of clients directly
      }

      // If the packet is a team-set - given the last player-ID of each team( TEAMSET16/32/48/63 )
      else if ( packetBufferString.substring(0, 7) == (String)NETMSG_TEAMSET )
      {
        packetBufferString = packetBufferString.substring(7);

        parseTeams( packetBufferString );
      }

      // If the packet is a Game Mode
      else if ( packetBufferString.substring(0, 8) == (String)NETMSG_GAMEMODE && gameMode == 0 )
      {
        packetBufferString = packetBufferString.substring(8);
        parseMode( packetBufferString );
      }

      // If the packet is a STARTGAME
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_STARTGAME )
      {
        packetBufferString = packetBufferString.substring(9);
        strip.clear();
        strip.show();
        if ( gameMode == 0 ) gameMode = 1; // default is Domination, if not chosen before start
        
        led_YES(50);
        if (gameStarted == 0) sendUdpBroadcast("STARTGAME");
        gameStarted = 1;
      }

      // If the packet is an ENDGAME
      else if ( packetBufferString.substring(0, 7) == (String)NETMSG_ENDGAME && gameStarted == 1 )
      {
        packetBufferString = packetBufferString.substring(7);
        operationMode = 42;
        sendUdpBroadcast("ENDGAME");

      }

      // If the packet is a RESET
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGRESET && operationMode == 42 )
      {
        packetBufferString = packetBufferString.substring(9);

        if ( operationMode = 42 ) sendUdpBroadcast("RESET");
        if ( server == 1 )
        {
          operationMode = 3;
        }
        else operationMode = 2;
        led_YES(50);
        resetFunction();
      }

      // If the packet is a REPLAY
      else if ( packetBufferString.substring(0, 6) == (String)NETMSG_REPLAY && operationMode == 42 )
      {
        packetBufferString = packetBufferString.substring(9);

        if ( operationMode = 42 ) sendUdpBroadcast("REPLAY");
        led_YES(50);
        replayFunction();
      }

      // If the packet is a SHOWFLAG
      else if ( packetBufferString.substring(0, 8) == (String)NETMSG_SHOWFLAG )
      {
        packetBufferString = packetBufferString.substring(9);

        if ( gameMode == 0 ) baseColor = white;
        else if ( gameMode > 0 ) baseColor = green;
        else baseColor = red;
        r_colorWipe( baseColor, 50);
        delay(1000);
        colorWipe( black, 50);
        delay(1000);
      }

      // If the packet is a FLAGTAKEN
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGTAKEN && gameMode == 5 )
      {
        packetBufferString = packetBufferString.substring(9);
        parseFlagCapture( packetBufferString );
      }

      // If the packet is a FLAGCAPTURED
      else if ( packetBufferString.substring(0, 12) == (String)NETMSG_FLAGCAPTURED && gameMode == 5 && flagTaken == 1)
      {
        packetBufferString = packetBufferString.substring(9);
        resetFlag;
      }

      // If the packet is a FLAGRESTORED
      else if ( packetBufferString.substring(0, 12) == (String)NETMSG_FLAGRESTORED && gameMode == 5 )
      {
        packetBufferString = packetBufferString.substring(9);
        flagOwner = 0;
      }



    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for separate team scores
void parseScore( String score )
{
  String score1 = score.substring(0, 3);
  teamScores[1] = score1.toInt();
  String score2 = score.substring(4, 7);
  teamScores[2] = score2.toInt();
  String score3 = score.substring(8, 11);
  teamScores[3] = score3.toInt();
  String score4 = score.substring(12, 15);
  teamScores[4] = score4.toInt();
  String score5 = score.substring(16, 19);
  maxPoints = score5.toInt();

  Serial.print( "max points = " );
  Serial.println( maxPoints );
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for number of players of teams
void parseTeams( String team )
{
  String teams1 = team.substring(0, 2);
  lastPlayerTeam[1] = teams1.toInt();
  String teams2 = team.substring(3, 5);
  lastPlayerTeam[2] = teams2.toInt();
  String teams3 = team.substring(6, 8);
  lastPlayerTeam[3] = teams3.toInt();
  String teams4 = team.substring(9, 11);
  lastPlayerTeam[4] = teams4.toInt();


  for ( int i = 1; i < 5 ; i++)
  {
    if ( lastPlayerTeam[i] > 0 )              // only display for teams > 0
    {
      Serial.print( "Last player Team " );
      Serial.print( i );
      Serial.print( " is ID" );
      Serial.println( lastPlayerTeam[i]);
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for points of a single team
void parsePoints( String teamPoints )
{
  String teamPoints1 = teamPoints.substring(0, 2);
  int i = teamPoints1.toInt();
  String teamPoints2 = teamPoints.substring(3, 5);
  int p = teamPoints2.toInt();

  teamScores[i] = teamScores[i] + p ;
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for the submitted game mode
void parseMode ( String UdpGameMode )
{
  String gMode = UdpGameMode.substring(0, 1);
  gameMode = gMode.toInt();
  led_YES(50);

  Serial.print( "Game Mode " );
  Serial.println( gameMode );
}

//-----------------------------------------------------------------------------------------------------------
void parseFlagCapture( String capture )
{
  String capture1 = capture.substring(0, 1);

  if ( baseTeam == capture1.toInt() )           // check if the flag has been taken by your team
  {
    String capture2 = capture.substring(1, 3);  // only if that is true,
    flagOwner = capture2.toInt();               // assign flagOwner - to be ignored by other teams
  }
}

//-----------------------------------------------------------------------------------------------------------
// Save received data to flag struct
void saveToFlag( String flagData )
{
  // Test if its a string we can handle, like 12:F4:8D:E3:47:03/000000058234/1
  if ( flagData.substring( 17, 18 ) == "/" and flagData.substring( 30, 31 ) == "/" )
  {
    String mac = flagData.substring( 0, 17 );
    // String hit_time = flagData.substring( 18, 30 ); This does not work because server and client times differ
    String hit_time = (String)currentTime;
    String team = flagData.substring( 31 );

    boolean found = false;
    int counter = 0;

    // Check if flag is already known
    for ( int count = 0; count < 8; count++ )
    {
      // If flag is known, use existing position
      if ( flags[count].mac == mac )
      {
        counter = count;
        found = true;
        if ( flags[counter].team != team )
        {
          Serial.print("Team ");
          Serial.print(teamColor[team.toInt()]);
          Serial.print(" took over flag ");
          Serial.print(count);
          Serial.print(" from team ");
          Serial.println(teamColor[flags[counter].team.toInt()]);
        }
      }
    }

    // If flag is unknown
    if ( found == false )
    {
      // Find first free position for new flag
      for ( int count = 0; count < 8; count++ )
      {
        if ( flags[count].mac.length() == 0 )
        {
          counter = count;
          break;
        }
      }
      Serial.println( "Saving new flag to first free position " + (String)counter );
    }
    // Save flag
    flags[counter].mac = mac;
    flags[counter].zeitpunkt = hit_time;
    flags[counter].team = team;
  }
}

//-----------------------------------------------------------------------------------------------------------
// Save received data to flag struct
void cleanupFlags()
{
  for ( int count = 0; count < 8; count++ )
  {
    long x = stringToLong(flags[count].zeitpunkt);      // Convert string timestamp of flags to long variable
    long y = currentTime - x;
    // If last update time of flag is > keepalive_wait_time remove the flag from the Flag struct
    if ( currentTime - x > keepalive_wait_time && x != 0 )
    {
      Serial.printf("No update received, removing flag %d flagtime is %d currentTime is %d\n", count, x, currentTime);
      flags[count].mac = "";
      flags[count].zeitpunkt = "";
      flags[count].team = "";
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Increment team scores for each flag owned
void incrementScore()
{
  String udp_string;

  int team_int;
  for ( int i = 0; i < MAX_FLAGS; i++ )
  {
    if ( flags[i].mac.length() > 0 and flags[i].team.toInt() > 0 )
    {
      team_int = flags[i].team.toInt();
      teamScores[ team_int ] = teamScores[ team_int ] + (loop_wait_time / 2000 );
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
void updateFlagScore()
{
  actTeamPoints = teamScores[actTeam];
}

//-----------------------------------------------------------------------------------------------------------
// Central server sends out updated score to all flags
void sendScore()
{
  /* Combine scores into one char buffer with the format 000/000/000/000/000/000/000/000/999
     Each triple digit number represents the points for one team, counting from team 1-8.
     The last triplet is the maximum points to be reached in the game.
     maxPoints is a Float variable and has to be converted into mPoints as integer.
  */
  char buf[40];
  sprintf (buf, "%03i/%03i/%03i/%03i/%03i", teamScores[1], teamScores[2], teamScores[3], teamScores[4], maxPoints );
  String message = NETMSG_FLAGSCORE + String(buf);
  sendUdpBroadcast(message);

  if ( debug == 1 ) Serial.println( message );
}

//-----------------------------------------------------------------------------------------------------------
// Convert a string into a Long variable
long stringToLong(String s)
{
  char arr[13];
  s.toCharArray(arr, sizeof(arr));
  return atol(arr);
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- IR Routines -------------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void printPacketInfo( byte * bits )                          // Decode the meaning of the bits in a packet into human-readable form
{
  byte a = bits[0], b = bits[1], c = bits[2], d = bits[3], e = bits[4], f = bits[5], g = bits[6], h = bits[7], i = bits[8], j = bits[9], k = bits[10], l = bits[11], m = bits[12], n = bits[13], o = bits[14], p = bits[15], q = bits[16], r = bits[17], s = bits[18], t = bits[19], u = bits[20];
  if ( e == 0 && f == 0 && g == 1 && h == 0 && i == 0 && j == 0 && a ^ b ^ c ^ d > 0 && k ^ l ^ m ^ n ^ o ^ p ^ q > 0 )
  {
    Serial.print( "gun shot " );
    Serial.print( shotIndex( bits + 1 ) );
    Serial.print( ", shooterID ");
    Serial.print( shooterIndex( bits + 11 ) );
    Serial.print( ", teamID ");
    Serial.print( teamIndex( bits + 11 ) );

    if ( gameStarted == 1)
    {
      updateActTeam();                                // recognition of team-id will be carried out on each detected shot

      if ( gameMode == 2 ) showBase();

      else if ( gameMode == 3 ) shootTheFlag();

      else if ( gameMode == 4 ) takehold();

      else if ( gameMode == 5 ) captureTheFlag();
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Update active team on flag
void updateActTeam()
{
  int ir_shooterID = shooterIndex( bits + 11 );    // detected shooterID from 1 - 64


  for ( int i = 1; i < 5; i++ )
  {
    if ( ir_shooterID > lastPlayerTeam[i - 1] && ir_shooterID <= lastPlayerTeam[i] )
    {
      Serial.print( "Team " );
      Serial.println( teamColor[i]);
      prevTeam = actTeam;
      actTeam = i;
    }
  }


  if ( ir_shooterID > 0 && ir_shooterID <= lastPlayerTeam[1] )
  {
    Serial.println("Team Red ");
    prevTeam = actTeam;
    actTeam = 1;
  }
  else if ( ir_shooterID > lastPlayerTeam[1] && ir_shooterID <= lastPlayerTeam[2] )
  {
    Serial.println("Team Blue ");
    prevTeam = actTeam;
    actTeam = 2;
  }
  else if ( ir_shooterID > lastPlayerTeam[2] && ir_shooterID <= lastPlayerTeam[3])
  {
    Serial.println("Team Green ");
    prevTeam = actTeam;
    actTeam = 3;
  }
  else if ( ir_shooterID > lastPlayerTeam[3] && ir_shooterID <= lastPlayerTeam[4] )
  {
    Serial.println("Team Purple ");
    prevTeam = actTeam;
    actTeam = 4;
  }


  if ( actTeam == 0)
  {
    Serial.println(", Error: Unknown Team ID ");
  }

  if ( actTeam != 0 )
  {
    led_colorActTeam = teamColorRGB[actTeam];
    actTeamPoints = teamScores[actTeam];
  }

  if ( (actTeam != prevTeam && gameMode != 2)  || (actTeam != prevTeam && gameMode != 4) )
  {
    led_TeamChange();
    delay (100);                                // clear the strip to allow the points to be displayed
    strip.clear();
    strip.show();
  }
}
