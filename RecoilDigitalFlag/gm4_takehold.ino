//-----------------------------------------------------------------------------------------------------------
//----------------------------------------- Game Mode Take & Hold -------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void gameModeTakeAndHold()
{
  if ( baseTeam == 0 && flagTaken == 0 ) displayAllColors();

  if ( baseTeam == 0 && flagTaken == 1 ) led_countUp();

  if ( baseTeam != 0 && flagTaken == 1 ) led_countDown();

  if ( ( currentTime - counterPreviousTime ) > loop_wait_time )
  {
    if ( operationMode == 3 )
    {
      // Server routines
      checkNetworkWinningCondition();         // Check winning condition and end game if condition is met
      incrementScore();                       // points will just be counted if flag is completely owned
      sendScore();                            // Broadcast current team scores to all flags
      cleanupFlags();                         // Remove flags which have not sent updates in
      serverFlagUpdate();                     // Update server flag locally instead of network communication
      // Server+Client routines
      updateFlagScore();                      // Update local flag score for team owning this flag
      identifyLeadingTeam();                  // Constantly look for the leading team
      displayOLEDScores();                    // Display scores on the OLED display
    }

    else if ( operationMode == 2 )
    {
      // Server+Client routines
      updateFlagScore();                      // Update local flag score for team owning this flag
      identifyLeadingTeam();                  // Constantly look for the leading team
      displayOLEDScores();                    // Display scores on the OLED display
    }
    counterPreviousTime = currentTime;
  }

}

//-----------------------------------------------------------------------------------------------------------
void takehold()
{
  if ( baseTeam == 0 && flagTaken == 0 )
  {
    baseColor  = teamColorRGB[actTeam];
    prevTeam = actTeam;                        // actual team is parked until color strip is full
    actTeam = 0;                               // is reset to start counting when color strip is full
    flagTaken = 1;
  }
  if ( baseTeam != 0 && flagTaken == 0 )
  {
    prevTeam = actTeam;                        // actual team is parked until color strip is full
    actTeam = baseTeam;                        // baseTeam is still in charge of the flag until color strip is empty
    flagTaken = 1;
  }
  if ( startCounting == 1 )                    // when flag is being shot at during change it will be recovered
  {
    baseColor  = teamColorRGB[actTeam];
    prevTeam = actTeam;
    baseTeam = 0;
    flagTaken = 1;
    Serial.println("Change interrupted");
  }
}

//-----------------------------------------------------------------------------------------------------------
void led_countUp()
{
  if ( startCounting == 0 )
  {
    c = 9;
    startCounting = 1;
    strip.fill( black, 0, strip.numPixels() );
    strip.show();
  }
  else if ( startCounting == 1 )
  {
    if ( currentTime - counterPreviousTime2 > 1000 )
    {
      strip.setPixelColor( c, teamColorRGB[ actTeam ] );
      strip.show();
      Serial.print("counting up pixel " );
      Serial.println(c);
      if ( c == 0 )
      {
        startCounting = 0;
        flagTaken = 0;
        baseTeam = actTeam;
        strip.fill( teamColorRGB[baseTeam], 0, strip.numPixels() );
        strip.show();
      }
      c--;
      counterPreviousTime2 = currentTime;
    }
  }
}


//-----------------------------------------------------------------------------------------------------------
void led_countDown()
{
  if ( startCounting == 0 )
  {
    c = 0;
    startCounting = 1;
    strip.fill( teamColorRGB[baseTeam], 0, strip.numPixels() );
    strip.show();
  }
  else if ( startCounting == 1 )
  {
    if ( currentTime - counterPreviousTime2 > 1000 )
    {
      strip.setPixelColor( c, black );
      strip.show();
      Serial.print("deleting pixel " );
      Serial.println(c);
      if ( c == 9 )
      {
        startCounting = 0;
        baseTeam = 0;
      }
      c++;
      counterPreviousTime2 = currentTime;
    }
  }
}
