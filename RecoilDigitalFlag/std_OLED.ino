//-----------------------------------------------------------------------------------------------------------
//---------------------------------------- Standard OLED Routines -------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void oledStartscreen()
{
  display.clear();
  display.display();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 0, "Welcome");
  display.setFont(ArialMT_Plain_10);
  display.drawString(63, 24, "to  the");
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 44, "DIGITAL FLAG");
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
void oled_OK()
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_24);
  display.drawString(63, 24, "OK");
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
void oled_NO()
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 30, "Not available");
  display.display();
}
