//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Include Libraries ---------------------------------------------
//-----------------------------------------------------------------------------------------------------------

//Flag is only working with ESP8266 board version 2.3.0, later seems not to work
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi
#include <ESP8266WiFi.h>
// Wifi 1.2.7 https://www.arduino.cc/en/Reference/WiFi
#include <ESP8266WiFiMulti.h>   // Include the Wi-Fi-Multi library
// https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/ESP8266WiFiMulti.h
#include <WiFiUdp.h>
// Driver for the SSD1306 and SH1106 based 128x64, 128x32, 64x48 pixel OLED display running on
// ESP8266/ESP32 Version 4.1.0 https://github.com/ThingPulse/esp8266-oled-ssd1306
#include <Wire.h>
#include "SSD1306Wire.h"                    // for OLED SSD1306 I2C 4-pin

//Include File system - needed for the internal SPIFFS
#include "FS.h"
// Include a setup file for configuration
#include "setup.h"

// web server and multicast DNS
#include <ESP8266mDNS.h>
WiFiServer server(80);

//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Global Definitions --------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Definition of global variables - values initialized in setup()
// Definition of global constants including values


//----- Network Definitions -----
const char* ssid     = STASSID;
const char* password = STAPSK;
const unsigned int localPort = 17500;               // local port to listen on - same as in the SimpleCoil app
const String mac_address = WiFi.macAddress();
const IPAddress broadcast(255, 255, 255, 255);      // Broadcast Address

int WifiStatus;                                     // the Wifi radio's status prior to connection
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1];      // buffers for receiving and sending data
WiFiUDP Udp;
ESP8266WiFiMulti wifiMulti;

//-----GLOBAL STRINGS-----
String MESSAGE_PREFIX    = "SimpleCoil:";
String NETMSG_STARTGAME  = "STARTGAME";
String NETMSG_ENDGAME    = "ENDGAME";
String NETMSG_FLAGALIVE  = "FLAGALIVE";
String NETMSG_FLAGSCORE  = "FLAGSCORE";
String NETMSG_FLAGPOINTS = "FLAGPOINTS";
String NETMSG_FLAGRESET  = "FLAGRESET";     // resetting all parameters
String NETMSG_REPLAY     = "REPLAY";        // just resetting counters
String NETMSG_TEAMSET    = "TEAMSET";
String NETMSG_ADDPOINTS  = "ADDPOINTS";     // new message e.g. "ADDPOINTS01/30" to add 30 points for team 1

//----- OLED Variables -----               // 4-pin I2C SSD 1306 OLED
#define SDA 14                             // SDA - Serial Data D6 - GPIO 14
#define SCL 12                             // SCL - Serial Clock D5 - GPIO 12
SSD1306Wire  display(0x3C, SDA, SCL);      // Address set here 0x3C is most common


// ----- Team declarations -----
const char *teamColor[] = {"None", "Red", "Blue", "Green", "Purple"};

// number of last player in each team; default values are:
unsigned int lastPlayerTeam[] = {0, 16, 32, 48, 63};

//----- Timing Definitions -----
const unsigned int loop_wait_time = 5000;                 // 5 seconds
const unsigned int keepalive_wait_time = 10000;           // 10 seconds
const unsigned long countIntervall = 1000;                // intervall in ms to count points
unsigned long counterPreviousTime;
unsigned long counterPreviousTime2;
unsigned long currentTime;

//----- Game Definitions -----
int operationMode;                                     // 3 = Server, 42 = Display Winner
int gameStarted;                                       // variable that indicates game has been started by button or by UDP message
int leadingScore;                                      // highest score during the game
int leadingTeam;                                       // team leading the game currently
int actTeam;                                           // last registered shot
int prevTeam;                                          // team that held the flag prior to a detected shot - can be the same team as actTeam
int baseTeam;                                          // team that owns the base
int teamID;

int maxPoints;                                         // limit for the maximum points, whoever reaches these points first, wins the game
int flagPoints;                                        // points set per flag * number of flags = maxPoints
int actTeamPoints;
int count;
int registeredFlags;

unsigned int teamScores[9];
struct Flag
{
  String mac;
  String zeitpunkt;
  String team;
};
const unsigned int MAX_FLAGS = 8;
Flag flags[MAX_FLAGS];


//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------Setup--------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
// Initialize global variables - this should be done here so a reset can be triggered via resetFunction calling setup()
void setup()
{
  Serial.begin( 115200 );                                  // Initialize Serial Console
  pinMode( LED_BUILTIN, OUTPUT );
  digitalWrite( LED_BUILTIN, HIGH );

  //----- Initialize internal SPIFFS storage -----
  if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
    return;
  }

  //----- Initialize OLED -----
  Wire.begin( SDA, SCL );
  display.init();
  display.flipScreenVertically();                          // turns the screen so that the contacs are above
  Serial.println( "OLED initialized." );


  // ----- Initialize WiFi -----
  // This is important because it clears still active entries on access points preventing reconnections
  WifiStatus = 0;
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  // standard WiFi networks - add additional Wi-Fi networks you want to connect to
  wifiMulti.addAP("Recoil Game Hub_WDS", "recoil123");
  wifiMulti.addAP("Recoil Game Hub", "recoil123");
  wifiMulti.addAP("Moria", ".Mellon.");

  Serial.println( "WiFi initialized." );




  //----- Initialize timing variables -----
  counterPreviousTime = 0;
  counterPreviousTime2 = 0;
  currentTime = millis();

  // ----- Initialize game variables -----
  operationMode = 3;
  gameStarted = 0;
  leadingScore = 0;
  leadingTeam = 0;
  actTeam = 0;
  prevTeam = 0;                                          // team that held the flag prior to a detected shot - can be the same team as actTeam
  baseTeam = 0;
  teamID = 0;

  actTeamPoints = 0;

  for ( int counter = 0; counter < 8; counter++ )       // debug for teams 1-8
  {
    flags[counter].mac = "";
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
    teamScores[counter] = 000;
  }
  Serial.println( "Game initialized." );
  display.clear();
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- Main Loop ---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void loop()
{
  currentTime = millis();                      // keep currentTime up-to-date
  udpListener();                               // listen to incoming UDP, process input and reply
  wifiConnect();                               // stay connected to the WiFi hub
  WiFiClient client = server.available();
  MDNS.update();
  if (client) handleClient(client);



  //--------------------------------------------- Server ----------------------------------------------------
  if ( operationMode == 3 )
  {
    if ( gameStarted == 0 ) serverReady();     // display on OLED


    if ( gameStarted == 1 ) serverCount();     // server is just counting points
  }

  //--------------------------------------------- End of game ----------------------------------------------
  else if ( operationMode == 42 )
  {
    displayOLEDScores();
  }
}

//-----------------------------------------------------------------------------------------------------------
//-------------------------------------------------- Server Main --------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void serverCount()
{
  if ( ( currentTime - counterPreviousTime ) > loop_wait_time ) // every 5 seconds
  {
    checkNetworkWinningCondition();         // Check winning condition and end game if condition is met
    incrementScore();                       // Increment all team scores according to current flag ownership
    sendScore();                            // Broadcast current team scores to all flags
    cleanupFlags();                         // Remove flags which have not sent updates in
    displayOLEDScores();                    // Display scores on the OLED display

    counterPreviousTime = currentTime;
  }
}

//-----------------------------------------------------------------------------------------------------------
//---------------------------------------------- Subroutines ------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void resetFunction() // resetting everything
{
  setup();
}

void replayFunction() // resetting counters
{
  operationMode = 3;
  gameStarted = 0;
  leadingScore = 0;
  leadingTeam = 0;
  actTeam = 0;
  prevTeam = 0;
  baseTeam = 0;
  teamID = 0;
  actTeamPoints = 0;

  for ( int counter = 0; counter < 8; counter++ )       // debug for teams 1-4
  {
    flags[counter].zeitpunkt = "";
    flags[counter].team = "";
    teamScores[counter] = 000;
  }
  display.clear();
  serverReady();
}

//-----------------------------------------------------------------------------------------------------------
void startGame()
{
  calculateMaxPoints();                               // calculating winning points bei number of flags
  Serial.print( "Max Points = " );
  Serial.println( maxPoints );
  gameStarted = 1;
  Serial.println( "Game started" );

  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 0, "Server ready");
  display.drawString(63, 24, "Game started");
  display.drawString(63, 44, "limit - " + String(maxPoints) + " points");
  display.display();

  delay(3000);
  display.clear();
}





//-----------------------------------------------------------------------------------------------------------
void checkNetworkWinningCondition()
{
  identifyLeadingTeam();
  if (leadingScore >= maxPoints)
  {
    sendScore();
    delay(1000);                         // wait for all flags receiving the end result
    sendUdpBroadcast( NETMSG_ENDGAME );
    operationMode = 42;                  // 42 = Endgame Mode
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 0, "Game over");
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_10);
    display.drawString(63, 16, "Team");
    display.setFont(ArialMT_Plain_24);
    display.drawString(63, 28, String(teamColor[leadingTeam]));
    display.setFont(ArialMT_Plain_10);
    display.drawString(63, 54, "has won");
    display.display();
    delay(3000);
    display.clear();


    Serial.print("Team ");
    Serial.print(teamColor[leadingTeam]);
    Serial.print(" has achieved ");
    Serial.print(leadingScore);
    Serial.println(" points!");
  }
}

//-----------------------------------------------------------------------------------------------------------
void identifyLeadingTeam()
{
  for ( int i = 1; i < sizeof(teamScores) / sizeof(teamScores[1]); i++ )
  {
    if ( teamScores[i] > leadingScore )
    {
      leadingScore = teamScores[i];
      leadingTeam = i;
    }
  }
  if (leadingScore != 0)
  {
    Serial.print( "Team ");
    Serial.print(teamColor[leadingTeam]);
    Serial.print(" is leading the pack with ");
    Serial.print(leadingScore);
    Serial.println(" points");
  }
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- OLED Routines -----------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void serverReady()
{
  String myIP = WiFi.localIP().toString();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_16);
  display.drawString( 63, 0, String(WiFi.SSID()) );
  display.drawString( 63, 24, "Server ready at" );
  display.drawString( 63, 44, myIP );
  display.display();
}

//-----------------------------------------------------------------------------------------------------------
void displayOLEDScores()
{
  if ( currentTime - counterPreviousTime2 >= countIntervall )
  {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 0, "Game running");
    display.display();

    display.setFont(ArialMT_Plain_10);

    if ( teamScores[1] > 0 )
    {
      display.setTextAlignment(TEXT_ALIGN_LEFT);
      display.drawString(10, 24, "1 Team Red");
      display.setTextAlignment(TEXT_ALIGN_RIGHT);
      display.drawString(120, 24, String(teamScores[1]));
    }
  }
  if ( teamScores[2] > 0 )
  {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 34, "2 Team Blue");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(120, 34, String(teamScores[2]));
  }
  if ( teamScores[3] > 0 )
  {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 44, "3 Team Green");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(120, 44, String(teamScores[3]));
  }
  if ( teamScores[4] > 0 )
  {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(10, 54, "4 Team Purple");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(120, 54, String(teamScores[4]));
  }
  display.display();
  counterPreviousTime2 = currentTime;
}

//-----------------------------------------------------------------------------------------------------------
//--------------------------------------------- WiFi Routines -----------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void wifiConnect()
{
  if ( WifiStatus != 3 )                       // not connected to Wifi
  {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 14, "Connecting to");
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 38, "WiFi");        // WiFi
    display.display();
    digitalWrite(LED_BUILTIN, HIGH);

    while (wifiMulti.run() != WL_CONNECTED)
    {
      yield();
      Serial.print(".");
      delay(1000);
    }
    Serial.println('\n');
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());              // Tell us what network we're connected to
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());           // Send the IP address of the ESP8266 to the computer

    server.begin();                           // start the web server
    MDNS.begin("setup");                      // create http://setup.local for internal website
    MDNS.addService("http", "tcp", 80);
    Udp.begin( localPort );
    Serial.printf( "UDP listening on port %d\n", localPort );

    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(63, 0, WiFi.SSID());
    display.drawString(63, 24, "connection");
    display.drawString(63, 44, "complete" );
    display.display();

    digitalWrite(LED_BUILTIN, LOW);
    delay(3000);
    display.clear();
  }
  WifiStatus = WiFi.status();                   // Constantly update WiFiStatus variable to re-enter WiFi connection when lost
}

//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------- Server Routines -------------------------------------------
//-----------------------------------------------------------------------------------------------------------
/* Introduction

  The game is client-server based, the flags normally start as clients, one flag can be defined as server.
  All flags send team changes as broadcasts. The server is listening to these packages and updates the states of all flags constantly.
  The server also regularly updates the teamScores and broadcasts the teamScores to all flags to update the flag colours.
  Flags which do not send a keepalive will be removed from the Flag struct.

  //-----------------------------------------------------------------------------------------------------------
  Game Data

  Two important game data variables are being maintained in this game.
  _________________________________________________________
  struct Flag
  {
  String mac;
  String zeitpunkt;
  String team;
  }

  The struct Flag contains all currently active Flags with the latest timestamp when they received an update.
  The variable starts with empty values and is subsequently filled and maintained.
  The mac address is being used as a unique identifier. Unknown macs are being added to the end of the struct
  Flags send event driven updates when their team changes, this updates the entry in the Flag struct.

  The transmitted strings via UDP have the following format: MAC/MILLISECONDS/TEAM.
  Example: 12:F4:8D:E3:47:03/000000058234/1
  mac: 17 characters
  zeit: 12 characters
  team: 1 character
  _________________________________________________________
  unsigned int teamScores[9]

  The variable teamScores contains all scores of all teams. A Time-phased event is checking which team owns which flag via incrementScore
  Every time incrementScore is executed every team receives points for every flag they currently own in the Flag construct.
  The countable teamScores go from 1-8. teamScore[0] is to be neglected as team 0 is "No team".

  //-----------------------------------------------------------------------------------------------------------
  Subroutine Overview

  udpListener()       Ongoing       Constantly listen to incoming UDP packages, distinguish packet types by packet size
  saveToFlag()        Event         through Flag changes, updating flags with changes
  incrementScore()    Time-phased   Incrementing team scores for each flag owned (loop_wait_time)
  sendScore()         Time-phased   Central server sending out updated score to all flags (loop_wait_time)
*/

//-----------------------------------------------------------------------------------------------------------
// Generic UDP broadcast function which appends a message_prefix and sends out a broadcast
void sendUdpBroadcast(String udp_string)
{
  Udp.beginPacket( broadcast, localPort );

  udp_string = MESSAGE_PREFIX + udp_string;

  for ( int i = 0; i < udp_string.length(); i++ )
  {
    Udp.write( udp_string[i] );
  }
  Udp.endPacket();
  delay(150);       // Without this delay of 100ms the UDP packets are not reliably sent. yield() is also not working.
}

//-----------------------------------------------------------------------------------------------------------
// Constantly listen to incoming UDP packages, distinguish packet types by packet size
void udpListener()
{
  // read the packet into packetBuffer
  int packetSize = Udp.parsePacket();
  int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
  packetBuffer[n] = 0;

  String packetBufferString = (String)packetBuffer;

  // If a packet is received
  if ( packetSize != 0)
  {
    // If the packet is for SimpleCoil
    if ( packetBufferString.substring(0, 11) == (String)MESSAGE_PREFIX)
    {
      packetBufferString = packetBufferString.substring(11);

      // If the packet is a flag keepalive
      if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGALIVE)
      {
        packetBufferString = packetBufferString.substring(9);
        saveToFlag( packetBufferString );
      }

      // If the packet is a points update
      if ( packetBufferString.substring(0, 9) == (String)NETMSG_ADDPOINTS)
      {
        packetBufferString = packetBufferString.substring(9);
        parsePoints( packetBufferString );
        displayOLEDScores();                                        // update points on display of clients directly
      }

      // If the packet is a team-set - given the last player-ID of each team( TEAMSET16/32/48/63 )
      else if ( packetBufferString.substring(0, 7) == (String)NETMSG_TEAMSET )
      {
        packetBufferString = packetBufferString.substring(7);
        Serial.println( packetBufferString );
        parseTeams( packetBufferString );
      }

      // If the packet is FLAGPOINTS - giving the points per used flag in the game to calculate the maxPoints
      else if ( packetBufferString.substring(0, 10) == (String)NETMSG_FLAGPOINTS )
      {
        packetBufferString = packetBufferString.substring(10);
        Serial.println( packetBufferString );
        parseFlagPoints( packetBufferString );
      }

      // If the packet is a STARTGAME
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_STARTGAME )
      {
        packetBufferString = packetBufferString.substring(9);

        startGame();
      }

      // If the packet is an ENDGAME
      else if ( packetBufferString.substring(0, 7) == (String)NETMSG_ENDGAME && gameStarted == 1 )
      {
        packetBufferString = packetBufferString.substring(7);
        operationMode = 42;
        Serial.println( "Game ended" );
      }

      // If the packet is a RESET
      else if ( packetBufferString.substring(0, 9) == (String)NETMSG_FLAGRESET && operationMode == 42 )
      {
        packetBufferString = packetBufferString.substring(9);
        Serial.println( "Game reset" );
        resetFunction();
      }

      // If the packet is a REPLAY
      else if ( packetBufferString.substring(0, 6) == (String)NETMSG_REPLAY && operationMode == 42 )
      {
        packetBufferString = packetBufferString.substring(9);
        Serial.println( "Counter reset" );
        replayFunction();
      }
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for separate team scores
void parseScore( String score )
{
  String score1 = score.substring(0, 3);
  teamScores[1] = score1.toInt();
  String score2 = score.substring(4, 7);
  teamScores[2] = score2.toInt();
  String score3 = score.substring(8, 11);
  teamScores[3] = score3.toInt();
  String score4 = score.substring(12, 15);
  teamScores[4] = score4.toInt();
  String score5 = score.substring(16, 19);
  maxPoints = score5.toInt();

  Serial.print( "max points = " );
  Serial.println( maxPoints );
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for number of players of teams TEAMSET16/32/48/63
void parseTeams( String team )
{
  String teams1 = team.substring(0, 2);
  lastPlayerTeam[1] = teams1.toInt();
  String teams2 = team.substring(3, 5);
  lastPlayerTeam[2] = teams2.toInt();
  String teams3 = team.substring(6, 8);
  lastPlayerTeam[3] = teams3.toInt();
  String teams4 = team.substring(9, 10);
  lastPlayerTeam[4] = teams4.toInt();


  for ( int i = 1; i < 5 ; i++)
  {
    if ( lastPlayerTeam[i] > 0 )              // only display for teams > 0
    {
      Serial.print( "Last player Team " );
      Serial.print( i );
      Serial.print( " is ID" );
      Serial.println( lastPlayerTeam[i]);
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for points of a single team
void parsePoints( String teamPoints )
{
  String teamPoints1 = teamPoints.substring(0, 1);
  int i = teamPoints1.toInt();
  String teamPoints2 = teamPoints.substring(1, 3);
  int p = teamPoints2.toInt();

  teamScores[i] = teamScores[i] + p ;

  Serial.print( "adding " );
  Serial.print( p );
  Serial.print( " points to team  " );
  Serial.println( i );
}

//-----------------------------------------------------------------------------------------------------------
// Parse the UDP score string for points of a single team
void parseFlagPoints( String PpFlag )
{
  String PpFlag1 = PpFlag.substring(0, 3);
  flagPoints = PpFlag1.toInt();

  Serial.print( "Points set to " );
  Serial.print( flagPoints );
  Serial.println( " per Flag  " );
}
//-----------------------------------------------------------------------------------------------------------
// Save received data to flag struct
void saveToFlag( String flagData )
{
  // Test if its a string we can handle, like 12:F4:8D:E3:47:03/000000058234/1
  if ( flagData.substring( 17, 18 ) == "/" and flagData.substring( 30, 31 ) == "/" )
  {
    String mac = flagData.substring( 0, 17 );
    // String hit_time = flagData.substring( 18, 30 ); This does not work because server and client times differ
    String hit_time = (String)currentTime;
    String team = flagData.substring( 31 );

    boolean found = false;
    int counter = 0;

    // Check if flag is already known
    for ( int count = 0; count < 8; count++ )
    {
      // If flag is known, use existing position
      if ( flags[count].mac == mac )
      {
        counter = count;
        found = true;
        if ( flags[counter].team != team )
        {
          Serial.print("Team ");
          Serial.print(teamColor[team.toInt()]);
          Serial.print(" took over flag ");
          Serial.print(count);
          Serial.print(" from team ");
          Serial.println(teamColor[flags[counter].team.toInt()]);
        }
      }
    }

    // If flag is unknown
    if ( found == false )
    {
      // Find first free position for new flag
      for ( int count = 0; count < 8; count++ )
      {
        if ( flags[count].mac.length() == 0 )
        {
          counter = count;
          break;
        }
      }
      Serial.println( "Saving new flag to first free position " + (String)counter );
    }
    // Save flag
    flags[counter].mac = mac;
    flags[counter].zeitpunkt = hit_time;
    flags[counter].team = team;

    if ( gameStarted == 0 ) countRegisteredFlags();
  }
}

//-----------------------------------------------------------------------------------------------------------
// Save received data to flag struct
void cleanupFlags()
{
  for ( int count = 0; count < 8; count++ )
  {
    long x = stringToLong(flags[count].zeitpunkt);      // Convert string timestamp of flags to long variable
    long y = currentTime - x;
    // If last update time of flag is > keepalive_wait_time remove the flag from the Flag struct
    if ( currentTime - x > keepalive_wait_time && x != 0 )
    {
      Serial.printf("No update received, removing flag %d flagtime is %d currentTime is %d\n", count, x, currentTime);
      flags[count].mac = "";
      flags[count].zeitpunkt = "";
      flags[count].team = "";
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Increment team scores for each flag owned
void incrementScore()
{
  String udp_string;

  int team_int;
  for ( int i = 0; i < MAX_FLAGS; i++ )
  {
    if ( flags[i].mac.length() > 0 and flags[i].team.toInt() > 0 )
    {
      team_int = flags[i].team.toInt();
      teamScores[ team_int ] = teamScores[ team_int ] + (loop_wait_time / 2000 );
    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// Central server sends out updated score to all flags
void sendScore()
{
  /* Combine scores into one char buffer with the format 000/000/000/000/999
     Each triple digit number represents the points for one team, counting from team 1-4.
     The last triplet is the maximum points to be reached in the game.
     maxPoints is a Float variable and has to be converted into mPoints as integer.
  */
  char buf[40];
  sprintf (buf, "%03i/%03i/%03i/%03i/%03i", teamScores[1], teamScores[2], teamScores[3], teamScores[4], maxPoints );
  String message = NETMSG_FLAGSCORE + String(buf);
  sendUdpBroadcast(message);

  if ( debug == 1 ) Serial.println( message );
}

//-----------------------------------------------------------------------------------------------------------
// Convert a string into a Long variable
long stringToLong(String s)
{
  char arr[13];
  s.toCharArray(arr, sizeof(arr));
  return atol(arr);
}

//-----------------------------------------------------------------------------------------------------------
void countRegisteredFlags()
{
  int count = 0;
  for (int i = 0; i < MAX_FLAGS; i++)
  {
    if (flags[i].mac.length() > 0) count++;   
  }
  registeredFlags = count;
}


// Calculate the maxPoints by the number of flags
void calculateMaxPoints() {
  int numberOfFlags = registeredFlags;
  maxPoints = flagPoints * numberOfFlags;
}

//-----------------------------------------------------------------------------------------------------------
//------------------------------------------- Web Server Routines -------------------------------------------
//-----------------------------------------------------------------------------------------------------------

void handleClient(WiFiClient client) {
  String request = client.readStringUntil('\r');
  client.flush();

  if (request.indexOf("GET / ") != -1 || request.indexOf("GET /index.html") != -1) {
    sendHtmlPage(client);
  }
  else if (request.indexOf("GET /setFlagPoints?value=") != -1) {
    String value = request.substring(request.indexOf('=') + 1, request.indexOf(' ', request.indexOf('=')));
    flagPoints = value.toInt();
    Serial.print( "Flag Points = " );
    Serial.println( flagPoints );
  }
  else if (request.indexOf("GET /GM1") != -1) {
    sendUdpBroadcast("GAMEMODE1");
  }
  else if (request.indexOf("GET /GM2") != -1) {
    sendUdpBroadcast("GAMEMODE2");
  }
  else if (request.indexOf("GET /GM3") != -1) {
    sendUdpBroadcast("GAMEMODE3");
  }
  else if (request.indexOf("GET /GM4") != -1) {
    sendUdpBroadcast("GAMEMODE4");
  }
  else if (request.indexOf("GET /GM5") != -1) {
    sendUdpBroadcast("GAMEMODE5");
  }
  else if (request.indexOf("GET /setTeamset?value=") != -1) {
    String value = request.substring(request.indexOf('=') + 1, request.indexOf(' ', request.indexOf('=')));
    sendUdpBroadcast("TEAMSET(" + value + ")");
  }
  else if (request.indexOf("GET /showFlags") != -1) {
    sendUdpBroadcast("SHOWFLAGS");
  }
  else if (request.indexOf("GET /startGame") != -1) {
    sendUdpBroadcast("STARTGAME");
    startGame();
  }
  else if (request.indexOf("GET /replay") != -1) {
    sendUdpBroadcast("REPLAY");
    replayFunction();
  }
  else if (request.indexOf("GET /reset") != -1) {
    sendUdpBroadcast("RESET");
    resetFunction();
  }
  client.stop();
}

void sendHtmlPage(WiFiClient client) {
  File file = SPIFFS.open("/index.html", "r");
  if (!file) {
    //client.print("HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/plain\r\n\r\n500 Internal Server Error");
    return;
  }

  //client.print("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n");
  while (file.available()) {
    client.write(file.read());
  }
  file.close();
}
