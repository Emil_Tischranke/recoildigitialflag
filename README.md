# RecoilDigitialFlag

Digital Flag Development for Recoil Lasertag Game.

Contains logic for IR receiver decryption, LED strips and network communication.


## Hardware

The hardware which the program is written for, is a ESP8266 - mainly WEMOS D1 Mini - but also the NodeMCU will work.
It might just be necessary to modify the built-in LED that indicates the WiFi connection.

The rest of the hardware consists of a TSOP4838 as IR sensor (940nm, 38kHz), a SDD1306 OLED Display (I2C) and a WS2812b LED strip.
The push button for the master can be any. The debouncing is carried out by the software.
The client flag just consists of the ESP, the IR sensor and the LED strip.

## Program

Up to version 1.2 we provided support for 8 teams with different colours. But this is not very realistic and we experienced problems in squeezing the SimpleCoil app to also assist 8 teams. 
So in the new revision 2 it will be just 4 teams. Additionally we will add a server program which will operate a single ESP8266 as dedicated server without any further hardware but an OLED display (could be on-board). This ESP might be located e.g. inside the Recoil Game Hub an will provide a webserver that allows the player (game master) to configure each single flag in the game (e.g. 2 base flags, 4 domination flags, 2 take&hold flags ....) over html.
Also we want to tie the flag program even tighter to the SimpleCoil app.



## English Program Description


The present Arduino program is designed for use with so-called Digital Flags, which are used in
interactive games, especially in the context of Recoil Lasertag games. These digital flags can be
"captured" by players in teams using infrared shots from lasertag guns. The program allows for
controlling and monitoring various game modes, where the flags can act either as standalone units or
networked as clients with a central server.


### Introduction and Setup

Upon powering on the Digital Flag, the setup is carried out first. This involves loading all necessary
libraries and initializing the hardware components such as the OLED display, LED strips, infrared
sensor, WiFi module, and butons. The global variables and constants are defined, including network
settings, team and game definitions, as well as timing parameters.


### Network Connection

The Digital Flags can connect to a network via WiFi. Several networks are supported, and the device
atempts to establish a connection to an available network. After a successful connection, the IP
address is displayed on the OLED display.


### Main Menu

After startup, the OLED display shows a main menu where the player can select the operating mode:
Standalone, WiFi Client, or WiFi Server. The selection is made using a connected buton. The LED
strips indicate the current selection by lighting up in the corresponding color.


### Game Modes Menu

In Standalone mode or as a server, the player can choose from various game modes: Domination,
Base Atack, Shoot the Flag, and Take & Hold. Each mode has different rules and victory conditions,
which are explained on the OLED display.


### Domination Mode

In Domination mode, teams collect points by maintaining control of the Digital Flags. The points are
updated at regular intervals and displayed on the OLED display as well as the LED strips. The team
that first reaches the maximum score wins the game.


### Base Attack Mode

In Base Atack mode, one team defends its base against atacks from other teams. The base has a
certain number of life points, which are reduced by hits from opposing teams. The atacking team
that captures the base wins the game.


### Shoot the Flag Mode

In Shoot the Flag mode, players must shoot the Digital Flag when it lights up in their team's color. Hits
in the correct color score points, while hits in the wrong color deduct points. The team with the most
points at the end of the game time wins.


### Take & Hold Mode

In Take & Hold mode, teams must hold the Digital Flag for a certain period to collect points. The team
that captures and holds the flag scores points, while the defending team tries to recapture the flag.


### Server and Clients

In network mode, one Digital Flag acts as a server, managing the game state and updating the teams'
scores. The other flags function as clients, sending their status changes to the server. The server
collects all information and sends regular updates to all clients.


### Game Start

The game can be started either by double-clicking the buton on the Digital Flag or by a UDP
command. All flags then synchronize and begin with the selected game mode.


### Gameplay

During the game, the Digital Flags respond to infrared shots from the players' lasertag guns. Hits are
detected and processed accordingly, depending on the current game mode. The LED strips display the
team colors and progress, while the OLED display shows the current scores and game information.


### Victory Conditions

Each game mode has specific victory conditions, which are explained on the OLED display. Once a
team meets the conditions, the game ends, and the winners are displayed on the displays and LED
strips.


### Reset and Restart

After the end of a game, the system can be reset to start a new game. This can be done either by a
long press on the buton or a corresponding UDP command.

The program offers a comprehensive and interactive experience for lasertag games and allows players
to experience various game modes and respond in real-time to changes in the game. The use of
network features expands the possibilities for organized games and tournaments.

  
  

## Deutsche Programmbeschreibung


Das vorliegende Arduino-Programm ist für die Verwendung mit sogenannten Digital Flags konzipiert, die in interaktiven Spielen, insbesondere im Rahmen von Recoil Lasertag-Spielen, eingesetzt werden. Diese digitalen Flaggen können von Spielern in Teams mittels Infrarot-Schüssen von Lasertag-Pistolen "erobert" werden. Das Programm ermöglicht es, verschiedene Spielmodi zu steuern und zu überwachen, wobei die Flaggen entweder als eigenständige Einheiten (Standalone) oder vernetzt als Clients mit einem zentralen Server agieren können.


### Einleitung und Setup

Beim Einschalten der Digital Flag wird zunächst das Setup durchgeführt. Hierbei werden alle notwendigen Bibliotheken geladen und die Hardware-Komponenten wie OLED-Display, LED-Streifen, Infrarot-Sensor, WiFi-Modul und Tasten initialisiert. Die globalen Variablen und Konstanten werden definiert, einschließlich Netzwerkeinstellungen, Team- und Spieldefinitionen sowie Timing-Parameter.


### Netzwerkverbindung

Die Digital Flags können sich über WiFi mit einem Netzwerk verbinden. Hierfür werden mehrere Netzwerke unterstützt, und das Gerät versucht, eine Verbindung zu einem verfügbaren Netzwerk herzustellen. Nach erfolgreicher Verbindung wird die IP-Adresse auf dem OLED-Display angezeigt.


### Hauptmenü

Nach dem Start zeigt das OLED-Display ein Hauptmenü an, in dem der Spieler den Betriebsmodus auswählen kann: Standalone, WiFi-Client oder WiFi-Server. Die Auswahl erfolgt über einen angeschlossenen Button. Die LED-Streifen zeigen die aktuelle Auswahl an, indem sie in der entsprechenden Farbe leuchten.


### Spielmodi-Menü

Im Standalone-Modus oder als Server kann der Spieler aus verschiedenen Spielmodi wählen: Domination, Base Attack, Shoot the Flag und Take & Hold. Jeder Modus hat unterschiedliche Regeln und Siegbedingungen, die auf dem OLED-Display erklärt werden.


### Domination-Modus

Im Domination-Modus sammeln Teams Punkte, indem sie die Kontrolle über die Digital Flags behalten. Die Punkte werden in regelmäßigen Abständen aktualisiert und auf dem OLED-Display sowie den LED-Streifen angezeigt. Das Team, das zuerst die maximale Punktzahl erreicht, gewinnt das Spiel.


### Base Attack-Modus

Im Base Attack-Modus verteidigt ein Team seine Basis gegen Angriffe der anderen Teams. Die Basis hat eine bestimmte Anzahl von Lebenspunkten, die durch Treffer der gegnerischen Teams reduziert werden. Das angreifende Team, das die Basis erobert, gewinnt das Spiel.


### Shoot the Flag-Modus

Im Shoot the Flag-Modus müssen die Spieler auf die Digital Flag schießen, wenn sie in der Farbe ihres Teams leuchtet. Treffer in der richtigen Farbe bringen Punkte, während Treffer in der falschen Farbe Punkte abziehen. Das Team mit den meisten Punkten am Ende der Spielzeit gewinnt.


### Take & Hold-Modus

Im Take & Hold-Modus müssen die Teams die Digital Flag für eine bestimmte Zeit halten, um Punkte zu sammeln. Das Team, das die Flagge erobert und hält, gewinnt Punkte, während das verteidigende Team versucht, die Flagge zurückzuerobern.


### Server und Clients

Im Netzwerkmodus agiert eine Digital Flag als Server, der den Spielstand verwaltet und die Punkte der Teams aktualisiert. Die anderen Flags fungieren als Clients, die ihre Statusänderungen an den Server senden. Der Server sammelt alle Informationen und sendet regelmäßig Updates an alle Clients.


### Spielstart

Das Spiel kann entweder durch einen Doppelklick auf den Button der Digital Flag oder durch einen UDP-Befehl gestartet werden. Alle Flags synchronisieren sich dann und beginnen mit dem gewählten Spielmodus.


### Spielablauf

Während des Spiels reagieren die Digital Flags auf Infrarot-Schüsse von den Lasertag-Pistolen der Spieler. Treffer werden erkannt und entsprechend verarbeitet, je nach aktuellem Spielmodus. Die LED-Streifen zeigen die Teamfarben und den Fortschritt an, während das OLED-Display die aktuellen Punktestände und Spielinformationen anzeigt.


### Siegbedingungen

Jeder Spielmodus hat spezifische Siegbedingungen, die auf dem OLED-Display erklärt werden. Sobald ein Team die Bedingungen erfüllt, endet das Spiel, und die Gewinner werden auf den Displays und LED-Streifen angezeigt.


### Reset und Neustart

Nach dem Ende eines Spiels kann das System zurückgesetzt werden, um ein neues Spiel zu starten. Dies kann entweder über einen langen Druck auf den Button oder einen entsprechenden UDP-Befehl erfolgen.


Das Programm bietet eine umfassende und interaktive Erfahrung für Lasertag-Spiele und ermöglicht es den Spielern, verschiedene Spielmodi zu erleben und in Echtzeit auf Änderungen im Spiel zu reagieren. Die Verwendung von Netzwerkfunktionen erweitert die Möglichkeiten für organisierte Spiele und Turniere.

#
#
# Bedienungsanleitung und Funktionsbeschreibung für das Lasertag-Spiel mit Digital Flags

## Einleitung
Das vorliegende Programm ist für ein Lasertag-Spiel konzipiert, bei dem Spieler mit "Recoil"-Pistolen auf digitale Flaggen (Digital Flags) schießen. Diese Flaggen sind mit verschiedenen Sensoren und Anzeigen ausgestattet, um Interaktionen im Spiel zu visualisieren und zu verarbeiten. Das Spiel bietet verschiedene Spielmodi (GameModes), unterstützt mehrere Spieler und Teams und verwendet farbige LED-Streifen, um die Teamzugehörigkeit und den Spielstatus anzuzeigen.

## Hardware-Komponenten
- ESP8266 Mikrocontroller
- Adafruit NeoPixel LED-Streifen
- OLED-Display (SSD1306)
- IR-Sensor
- WiFi-Modul für Netzwerkfunktionalität
- Taster (OneButton-Bibliothek)

## Software-Komponenten und Bibliotheken
- Adafruit_NeoPixel für LED-Streifen
- ESP8266WiFi und ESP8266WiFiMulti für WiFi-Konnektivität
- WiFiUdp für UDP-Kommunikation
- OneButton für Taster-Interaktionen
- SSD1306Wire für OLED-Display

## Spielmodi (GameModes)
Das Spiel unterstützt verschiedene Modi:
1. Domination: Teams kämpfen um die Kontrolle über Flaggen, um Punkte zu sammeln.
2. Base Attack: Ein Team verteidigt seine Basis gegen Angriffe der anderen Teams.
3. Shoot the Flag: Spieler schießen auf eine Flagge, die zufällig ihre Farbe ändert, um Punkte zu erzielen.
4. Take & Hold: Teams versuchen, eine Flagge für eine bestimmte Zeit zu halten.

## Spieleranzahl und Teams
Das Spiel kann eine variable Anzahl von Spielern und Teams unterstützen. Die Teams sind durch Farben gekennzeichnet, die durch die LED-Streifen dargestellt werden. Die Farben sind:
- Rot
- Blau
- Grün
- Lila

## Programmstruktur und Funktionen

### setup()
Initialisiert alle Hardware-Komponenten, stellt WiFi-Verbindungen her und setzt globale Variablen zurück. Dieser Abschnitt wird einmal beim Start des Mikrocontrollers ausgeführt.

### loop()
Die Hauptfunktion des Programms, die kontinuierlich ausgeführt wird. Sie überwacht Tastendrücke, liest IR-Signale, aktualisiert die Anzeige und steuert die Spiellogik abhängig vom aktuellen Spielmodus und Betriebsmodus.

### resetFunction()
Setzt das Spiel zurück und initialisiert das Setup erneut.

### flagReady()
Bereitet die Flagge vor und zeigt an, dass das Spiel mit einem Doppelklick gestartet werden kann.

### singleclick(), doubleclick(), longPressStart()
Funktionen, die durch Tasteninteraktionen ausgelöst werden und Aktionen wie Spielstart oder Reset ausführen.

### gameModeDomination(), gameModeBaseAttack(), gameModeShootTheFlag(), gameModeTakeAndHold()
Diese Funktionen steuern die spezifische Logik für jeden Spielmodus, einschließlich Punktezählung, Anzeigeaktualisierungen und Netzwerkkommunikation.

### wifiConnect(), sendUdpBroadcast(), udpListener()
Funktionen für die Netzwerkkommunikation, die es ermöglichen, Spielinformationen zwischen den Flaggen auszutauschen.

### printResults(), readIR(), printPacketInfo()
Funktionen zum Lesen und Interpretieren von IR-Signalen, die von den "Recoil"-Pistolen gesendet werden.

### colorWipe(), rainbow(), led_YES(), led_NO()
Verschiedene LED-Animationen, die für visuelles Feedback im Spiel verwendet werden.

### oledStartscreen(), oled_OK(), oled_NO()
Funktionen zur Steuerung des OLED-Displays, um Informationen und Statusmeldungen anzuzeigen.

## Spielstart und -ende
Das Spiel beginnt mit einem Doppelklick auf den Taster einer Flagge oder durch eine Netzwerknachricht. Es endet, wenn ein Gewinnzustand erreicht ist (z.B. ein Team erreicht die maximale Punktzahl) oder durch eine Netzwerknachricht, die das Ende des Spiels signalisiert.

## Parameter
Verschiedene Parameter wie `maxPoints`, `teamScores` und `operationMode` steuern den Spielablauf und die Logik für die Punktezählung und Spielmodi.

## Netzwerkkommunikation
Die Flaggen kommunizieren über das Netzwerk, um Spielstände zu synchronisieren und den Spielstatus zu aktualisieren. Dies geschieht über UDP-Nachrichten, die spezifische Befehle und Daten enthalten.

## Zusammenfassung
Diese Bedienungsanleitung bietet einen umfassenden Überblick über die Funktionsweise des Lasertag-Spiels mit Digital Flags. Sie beschreibt die verschiedenen Spielmodi, die Teamstruktur und die Interaktionen zwischen den Spielern und den digitalen Flaggen. Die detaillierte Beschreibung der einzelnen Funktionen und Routinen ermöglicht es den Benutzern, das Spiel zu verstehen und zu bedienen. 


# Einzelbeschreibung der Unterprogramme

**resetFunction()**
Diese Funktion dient dazu, das System zurückzusetzen und alle Variablen und Einstellungen auf ihre Anfangswerte zu initialisieren. Sie wird aufgerufen, wenn der Benutzer einen langen Druck auf den OneButton ausführt (ACTION_LONGSTART). Die Funktion gibt eine Meldung über den Reset-Vorgang auf der seriellen Konsole aus, löscht das OLED-Display und zeigt eine Reset-Nachricht an. Anschließend ruft sie die `setup()`-Funktion auf, um alle globalen Variablen und Hardware-Komponenten neu zu initialisieren. Es gibt keine Parameter und die Funktion wird intern von keiner anderen Funktion aufgerufen.

**flagReady()**
Diese Funktion wird aufgerufen, wenn das Gerät in den Bereitschaftsmodus versetzt wird und auf den Start des Spiels wartet. Sie zeigt auf dem OLED-Display an, dass das Gerät bereit ist und auf einen Doppelklick wartet, um das Spiel zu starten. Wenn ein Doppelklick erkannt wird (ACTION_DOUBLE), wird die Variable `gameStarted` auf 1 gesetzt, das OLED-Display wird gelöscht und eine UDP-Broadcast-Nachricht mit dem Startsignal wird gesendet. Die Funktion wird im Hauptloop aufgerufen, wenn `operationMode` auf 2 (Client) oder 3 (Server) gesetzt ist und das Flag noch nicht positioniert ist (`flagInPosition` == 0).

**singleclick()**
Diese Funktion wird als Callback für den OneButton definiert und aufgerufen, wenn ein einfacher Klick erkannt wird. Sie setzt die Variable `nextAction` auf ACTION_SINGLE. Diese Variable wird in anderen Teilen des Programms verwendet, um auf Benutzereingaben zu reagieren. Die Funktion hat keine Parameter und gibt keine Werte zurück.

**doubleclick()**
Ähnlich wie `singleclick()`, wird diese Funktion als Callback für den OneButton definiert und aufgerufen, wenn ein Doppelklick erkannt wird. Sie setzt die Variable `nextAction` auf ACTION_DOUBLE. Diese Variable wird in anderen Teilen des Programms verwendet, um auf Benutzereingaben zu reagieren. Die Funktion hat keine Parameter und gibt keine Werte zurück.

**longPressStart()**
Diese Funktion wird als Callback für den OneButton definiert und aufgerufen, wenn ein langer Druck erkannt wird. Sie setzt die Variable `nextAction` auf ACTION_LONGSTART. Diese Variable wird verwendet, um die `resetFunction()` aufzurufen und das System zurückzusetzen. Die Funktion hat keine Parameter und gibt keine Werte zurück.

**countPoints()**
Diese Funktion wird im Standalone-Modus (operationMode == 1) aufgerufen und ist für das Zählen der Punkte im Spielmodus "Domination" verantwortlich. Sie überprüft, ob das aktuelle Team (`actTeam`) Punkte verdient hat, und aktualisiert die Punktzahl (`teamScores[actTeam]`) entsprechend. Die Funktion wird regelmäßig im Hauptloop aufgerufen und verwendet die Zeitintervallvariable `countIntervall`, um zu bestimmen, wann Punkte hinzugefügt werden sollen. Sie aktualisiert auch die LED-Anzeige, um den Fortschritt anzuzeigen, und überprüft die Gewinnbedingungen.

**displayOLEDScores()**
Diese Funktion aktualisiert das OLED-Display mit den aktuellen Punktzahlen der Teams. Sie wird regelmäßig im Hauptloop aufgerufen und zeigt die Punktzahlen der Teams an, die Punkte erzielt haben. Die Funktion verwendet die Variable `teamScores` und zeigt die Punktzahlen für jedes Team an. Sie wird von `countPoints()` und anderen Spielmodus-Funktionen aufgerufen.

**displayLEDProgress()**
Diese Funktion aktualisiert die LED-Anzeige, um den Fortschritt des Teams, das das Flag besitzt, zu zeigen. Sie wird von `countPoints()` aufgerufen und verwendet die Variablen `led_Quotient` und `led_Percent`, um zu bestimmen, wie viele LEDs in der Farbe des aktiven Teams (`led_colorActTeam`) leuchten sollen. Die Funktion wird im Standalone-Modus und im Netzwerkmodus verwendet, um den Fortschritt anzuzeigen.

**printGameResults()**
Diese Debug-Funktion gibt die aktuellen Spielresultate auf der seriellen Konsole aus. Sie wird von `displayOLEDScores()` aufgerufen, wenn die Debug-Variable (`debug`) auf 1 gesetzt ist. Sie gibt die Punktzahlen für jedes Team und die Berechnung des LED-Fortschritts aus.

**checkWinningCondition()**
Diese Funktion überprüft, ob ein Team die maximale Punktzahl (`maxPoints`) erreicht hat, und setzt das Spiel in den Endmodus (operationMode = 42), wenn dies der Fall ist. Sie wird von `countPoints()` aufgerufen und verwendet die Variable `actTeamPoints`, um die Punktzahl des aktiven Teams zu überprüfen. Wenn die Bedingung erfüllt ist, wird eine UDP-Broadcast-Nachricht mit dem Endsignal gesendet.

**checkNetworkWinningCondition()**
Ähnlich wie `checkWinningCondition()`, aber speziell für den Netzwerkmodus. Diese Funktion überprüft, ob das führende Team (`leadingTeam`) die maximale Punktzahl erreicht hat, und setzt das Spiel in den Endmodus, wenn dies der Fall ist. Sie wird von `gameModeDomination()` aufgerufen und verwendet die Variable `leadingScore`, um die höchste Punktzahl zu überprüfen.

**displayWinner()**
Diese Funktion zeigt den Gewinner des Spiels auf der LED-Anzeige an, indem sie die LEDs in der Farbe des führenden Teams leuchten lässt. Sie wird aufgerufen, wenn das Spiel in den Endmodus versetzt wird (operationMode = 42). Sie verwendet die Variable `leadingTeam`, um die Farbe des führenden Teams zu bestimmen, und die Variable `currentTime`, um die Anzeige zu aktualisieren.

**identifyLeadingTeam()**
Diese Funktion identifiziert das führende Team, indem sie die Punktzahlen aller Teams (`teamScores`) durchgeht und das Team mit der höchsten Punktzahl findet. Sie wird regelmäßig von `gameModeDomination()` aufgerufen und aktualisiert die Variablen `leadingScore` und `leadingTeam`.

**showBase()**
Diese Funktion wird im Spielmodus "Base Attack" aufgerufen und zeigt den Zustand der Basis an. Sie wird von `updateActTeam()` aufgerufen, wenn ein Schuss erkannt wird, und aktualisiert die Variablen `baseTeam`, `baseColor`, `baseHit` und `baseHeal`, um den Zustand der Basis zu reflektieren.

**countHitPoints()**
Diese Funktion wird im Spielmodus "Base Attack" aufgerufen und zählt die Trefferpunkte der Basis. Sie wird regelmäßig im Hauptloop aufgerufen und aktualisiert das OLED-Display mit der verbleibenden Gesundheit der Basis und der verbleibenden Spielzeit. Sie verwendet die Variablen `actHitPoints`, `maxHitPoints`, `baseHit`, `baseHeal`, `startTime` und `playTimeInMillis`.

**displayBaseHealth()**
Diese Funktion wird im Spielmodus "Base Attack" aufgerufen und zeigt die Gesundheit der Basis auf der LED-Anzeige an. Sie wird von `countHitPoints()` aufgerufen und verwendet die Variable `actHitPoints`, um zu bestimmen, wie viele LEDs in der Farbe der Basis (`baseColor`) leuchten sollen.

**randomizeColors()**
Diese Funktion wird im Spielmodus "Shoot the Flag" aufgerufen und wählt zufällige Farben für das Flag aus. Sie wird regelmäßig im Hauptloop aufgerufen und verwendet die Variable `changeTime`, um zu bestimmen, wann die Farbe geändert werden soll. Sie aktualisiert auch die Variable `baseColor`, um die aktuelle Farbe des Flags zu speichern.

**teamAddPoint()**
Diese Funktion wird im Spielmodus "Shoot the Flag" aufgerufen und fügt einem Team Punkte hinzu, wenn es das Flag mit der richtigen Farbe trifft. Sie wird von `shootTheFlag()` aufgerufen und verwendet die Variable `randomNumber`, um zu überprüfen, ob die Farbe des Teams mit der Farbe des Flags übereinstimmt.

**shootTheFlag()**
Diese Funktion implementiert die Logik des Spielmodus "Shoot the Flag". Sie wird aufgerufen, wenn ein Schuss erkannt wird, und verwendet `teamAddPoint()`, um Punkte hinzuzufügen oder abzuziehen. Sie aktualisiert auch das OLED-Display mit der aktuellen Punktzahl und der Team-ID.

**wifiConnect()**
Diese Funktion stellt eine Verbindung zu einem WiFi-Netzwerk her. Sie wird im Hauptloop aufgerufen, wenn das Gerät nicht mit dem WiFi verbunden ist (`WifiStatus != 3`). Sie zeigt während des Verbindungsaufbaus ein grün-blaues Laufband an (`colorBand_bg`) und startet bei erfolgreicher Verbindung den UDP-Service (`Udp.begin`) und schaltet die on-board LED ein (`LED_BUILTIN, LOW`).